package es.upv.elight;

import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.things.pio.PeripheralManager;
import com.google.android.things.pio.UartDevice;
import com.google.android.things.pio.UartDeviceCallback;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import es.upv.iilumination_2.R;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class Communications {


    private UartDevice uart;

    private boolean fetched; //Variable para comprobar que se han actualizado los datos correctamente del monitor serie
    private final int  fetch_millis = 3000; //Tiempo entre comprobaciones en milisegundos

    private ArrayList<String> UartBuffer = new ArrayList<String>();
    private String UartReadData;
    public static Map<String, Date> last_updates = new HashMap<>();

    public  RecyclerView recycler;
    public  NodoAdapter adapter;
    public  GridLayoutManager lManager;

    public List<DocumentReference> Nodos_fireStore;
    public List<Nodo> NodosLista;
    public FirebaseFirestore fb;

    public Communications(String uartName, RecyclerView rv, NodoAdapter na, GridLayoutManager glm, List<Nodo> NodosLista)
            throws IOException {
        crearUART(uartName);
        this.Nodos_fireStore = new ArrayList<>();

        this.NodosLista = NodosLista;
        this.recycler = rv; this.adapter = na; this.lManager = glm;

        getNodesFromFirestore();

        Handler r_handler = new android.os.Handler();

        //Hilo de ejecución aparte para leer del monitor serie
        Runnable r_secondary = new Runnable() {

            @Override
            public void run() {
                JSONObject jo = leerJSON();
                if(jo!=null && !fetched) {
                    Log.d(TAG,"datos detectados!");
                    procesarDatosRecibidos(jo);
                }
                r_handler.postDelayed(this, fetch_millis);
            }
        };
        r_handler.postDelayed(r_secondary, 1000);


    }


    /**
     * Se procesan los callbacks al capturar datos por el puerto serie
     */
    private UartDeviceCallback uartCallback = new UartDeviceCallback() {
        @Override
        public boolean onUartDeviceDataAvailable(UartDevice uart) {

            fetched = true; //lectura realizada

             Log.d(TAG, "Recibido de Arduino callback --> "+UartReadData);
            //Procesa los datos recibidos para ser mostrados y enviados almacenados en la BBDD
            JSONObject jo = leerJSON();
            procesarDatosRecibidos(jo);

            // Continue listening for more interrupts
            return true;
        }

        @Override
        public void onUartDeviceError(UartDevice uart, int error) {
            Log.w(TAG, uart + ": Error event " + error);
        }
    };


    /**
     * Función para ller los nodos de FireStore y registrar callbacks,
     * si se ha modificado uno de estos, se actualiza su información enviandola por UART
     */
    public void readNodes(){

        //Se leen todos los nodos del sistema (En el futuro deben de leerse solo los del usuario autentificado)
        this.fb = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        fb.setFirestoreSettings(settings);

        CollectionReference cr = fb.collection("nodos");
        cr.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){

                    for(QueryDocumentSnapshot doc: task.getResult()){
                        Log.d("FALLA", doc.getReference().toString());

                        Nodos_fireStore.add(doc.getReference());

                        doc.getReference().addSnapshotListener(new EventListener<DocumentSnapshot>() {
                            @Override
                            public void onEvent(@Nullable DocumentSnapshot snapshot, @Nullable FirebaseFirestoreException e) {
                                if (e != null) {
                                    Log.w(TAG, "Listen failed.", e);
                                    return;
                                }

                                if (snapshot != null && snapshot.exists()) {

                                        Nodo n = snapshot.toObject(Nodo.class);

                                        if(n.isUpdatedViaMobile()){ //Si se actualiza desde un móvil entonces se envía a los arduinos
                                            Gson gson = new Gson();
                                            String json = gson.toJson(n);

                                            boolean previouslySended= false;
                                            //Comprueba que no se haya enviado anteriorirmente
                                            for (Map.Entry<String, Date> entry : last_updates.entrySet()) {
                                                String key = entry.getKey();
                                                Date value = entry.getValue();

                                                if(n.getMAC().equals(key) && value.toString().equals(n.getMomento().toString())){
                                                    previouslySended = true;
                                                    Log.d("Fechas iguales", "Envio ignorado");
                                                }
                                            }

                                            //Si no se ha enviado previamente ~
                                           // if (!previouslySended) {
                                                Log.d("Nodo Capturado MAC", n.getMAC());
                                                Log.d(TAG, "Current data: " + json);

                                                last_updates.put(n.getMAC(), n.getMomento());

                                                escribirUART(json);
                                            //}

                                        }

                                } else {
                                    Log.d(TAG, "Current data: null");
                                }
                            }
                        });
                    }
                    //Fin del bucle
                }
            }
        });



    }



    public void registerCallbacks(){
        try {

            this.uart.registerUartDeviceCallback(uartCallback);
            Log.d(TAG, "Callbacks para la UART registrados");

        } catch (IOException e) {
            Log.w(TAG, "Error registrando callback para la UART");
        }


    }

    /**
     * Procesa datos de un JSON mediante un Objeto POJO de tipo Nodo
     * estos se suben a la base de datos en Firestore
     * @param jo
     */
    public void procesarDatosRecibidos(JSONObject jo){

        try {


            //Log.d("Nodo", "Creando nodo: "+jo.toString());

            // Creando la referencia de Firestore al nodo que se va a actualizar
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                    .setTimestampsInSnapshotsEnabled(true)
                    .build();
            db.setFirestoreSettings(settings);


            //Solicitud de datos de un nodo
            String f_json = jo.toString();
            if (f_json.contains("RetrieveData")) {
                    String m_mac = jo.getString("MAC");
                    db.collection("nodos").whereEqualTo("mac", m_mac)
                            .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                    if (task.isSuccessful()){
                                        for(QueryDocumentSnapshot doc: task.getResult()){

                                            Nodo n = doc.toObject(Nodo.class); //Se obtiene la copia de firestore

                                           Gson g = new Gson();
                                           String json = g.toJson(n); //se pasa a JSON
                                           escribirUART(json); //Se escribe por la UART
                                           last_updates.put(n.getMAC(), n.getMomento());

                                            Log.d(TAG, "Datos a una petición atendidos satisfactoriamente.");
                                            Log.d(TAG, "Datos: "+json);


                                        }
                                    }else {
                                        Log.w(TAG, "Datos no actualizados a la petición de un nodo!");
                                    }
                                }
                            });
            }

            else {
                //Modificación de un nodo


                DocumentReference dr = db.collection("nodos").document(jo.getString("MAC"));

                // Creación del POJO con información básica, MAC, nombre, estado de la iluminación y si el encendido es automático o no ////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                boolean estado = false;
                if (jo.getString("Status").equals("Encendido")) {
                    estado = true;
                }
                Log.d("Estado enviado", String.valueOf(estado));
                Nodo n = new Nodo(jo.getString("MAC"), jo.getString("Node"), estado, jo.getBoolean("Auto"));
                n.setUserId(MainActivity.userId);
                Date momento = new Date();
                n.setMomento(momento);
                n.setEstado(estado);

                // Estados booleanos /////////////////////////////////////////
                //////////////////////////////////////////////////////////////

                n.setColor_custom(jo.getBoolean("color_custom")); //Color personalizado
                n.setIntensidad_auto(jo.getBoolean("intensidad_auto")); // Intensidad automática/fija
                n.setUpdatedViaMobile(jo.getBoolean("updatedViaMobile")); // Datos actualizados desde los nodos o desde el móvil

                ////////////////// Procesa el color ///////////////////////////
                ///////////////////////////////////////////////////////////////
                List<Integer> m_color = new ArrayList<Integer>();
                ;
                JSONArray colors = jo.getJSONArray("color");
                int R = colors.getInt(0);
                int G = colors.getInt(1);
                int B = colors.getInt(2);

                m_color.add(R);
                m_color.add(G);
                m_color.add(B);
                n.setColor(m_color);

                double Wconsumidos = calculateConsumtion(R, G, B);

                // Actualización de los consumos ////////////////////////////
                ///////////////////////////////////////////////////////////////
                Consumos c = new Consumos(Wconsumidos, momento, n.getEstado());
                c.setProcesado(false); //Consumo sin precio calculador

                dr.collection("consumos").add(c);


                // Actualiza los datos en firestore ////////////////////////////
                ///////////////////////////////////////////////////////////////


                dr.set(n).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Fallo al actualizar datos");
                    }
                }).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            getNodesFromFirestore();
                            Log.d(TAG, "Datos actualizados con éxito en firestore");
                        }

                        if (task.getException() != null) {
                            Exception ex = task.getException();
                            ex.printStackTrace();
                        }

                    }
                });


                //Se refrescan los precios cuando se apaga el nodo
                if (!n.getEstado()) {
                    calculateCostDaily(dr);
                }

            } //Acaba la actualización (si no es una solicitud de datos


            } catch(JSONException e){
                Log.w(TAG, uart + ": Fallo al crear Nodo. " + e);

            } finally{

                this.fetched = false; //lectura terminada
            }

    }


    /**
     * Funcion para actualizar el esado de un nodo de firestore,
     * la actualizacion se propaga al nodo correspondiente tambien
     * @param mac
     * @param estado
     */
    public static void setNodeEstado(String mac, boolean estado){

        FirebaseFirestore fb = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        fb.setFirestoreSettings(settings);

        fb.collection("nodos").whereEqualTo("mac", mac).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    for(QueryDocumentSnapshot doc: task.getResult()){
                        Nodo n = doc.toObject(Nodo.class);
                        n.setUpdatedViaMobile(true);
                        n.setAutomatico(false);

                        n.setEstado(estado);
                        doc.getReference().set(n);
                    }
                }
            }
        });
    }


    /**
     * Funcion para actualizar el encendido automatico de un nodo de firestore,
     * la actualizacion se propaga al nodo correspondiente tambien
     * @param mac
     * @param estado
     */
    public static void setNodeAuto(String mac, boolean estado){

        FirebaseFirestore fb = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        fb.setFirestoreSettings(settings);

        fb.collection("nodos").whereEqualTo("mac", mac).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    for(QueryDocumentSnapshot doc: task.getResult()){
                        Nodo n = doc.toObject(Nodo.class);
                        n.setUpdatedViaMobile(true);

                        n.setEstado(false);
                        n.setAutomatico(estado);
                        doc.getReference().set(n);
                    }
                }
            }
        });
    }



    /**
     * Funcion para actualizar el nombre de un nodo en firestore,
     * la actualizacion se propaga al nodo correspondiente tambien
     *
     * @param mac
     * @param nombre
     */
    public static void setNodeName(String mac, String nombre){

        FirebaseFirestore fb = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        fb.setFirestoreSettings(settings);

        fb.collection("nodos").whereEqualTo("mac", mac).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    for(QueryDocumentSnapshot doc: task.getResult()){
                        Nodo n = doc.toObject(Nodo.class);
                        n.setUpdatedViaMobile(true);

                        n.setNombre(nombre);
                        doc.getReference().set(n);
                    }
                }
            }
        });
    }


    /**
     * Funcion para actualizar el color RGB  de un nodo en firestore,
     * la actualizacion se propaga al nodo correspondiente tambien
     *
     * @param mac
     * @param rgb
     */
    public static void setNodeCustomColor(String mac, List<Integer> rgb){

        FirebaseFirestore fb = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        fb.setFirestoreSettings(settings);

        fb.collection("nodos").whereEqualTo("mac", mac).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    for(QueryDocumentSnapshot doc: task.getResult()){
                        Nodo n = doc.toObject(Nodo.class);
                        n.setUpdatedViaMobile(true);

                        n.setColor(rgb);
                        n.setColor_custom(true);

                        doc.getReference().set(n);
                    }
                }
            }
        });
    }




    /**
     * Funcion para actualizar el estoad automatico de la regulacion de la intensidad de un nodo de firestore,
     * la actualizacion se propaga al nodo correspondiente tambien
     * @param mac
     * @param estado
     */
    public static void setNodeIntensityAuto(String mac, boolean estado){

        FirebaseFirestore fb = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        fb.setFirestoreSettings(settings);

        fb.collection("nodos").whereEqualTo("mac", mac).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    for(QueryDocumentSnapshot doc: task.getResult()){
                        Nodo n = doc.toObject(Nodo.class);
                        n.setUpdatedViaMobile(true);

                        n.setIntensidad_auto(estado);
                        doc.getReference().set(n);
                    }
                }
            }
        });
    }





    /**
     * Funcion para obtener una lista de nodos
     * en funcion de lo que se encuentra en Firestore
     * @return
     */
    public List<Nodo> getNodesFromFirestore(){
        List<Nodo> lnodos = new ArrayList<>();

        NodosLista.clear();

        this.fb = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        fb.setFirestoreSettings(settings);

        fb.collection("nodos").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            lnodos.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Nodo n = document.toObject(Nodo.class);
                                lnodos.add(n);
                            }
                            NodosLista.clear();
                            NodosLista.addAll(lnodos);
                            adapter.notifyDataSetChanged();

                            Log.d(TAG, "leyendo lista completa con exito ");

                        } else {
                            Log.d(TAG, "Error leyendo lista completa: ", task.getException());
                        }
                    }
                });



        return lnodos;
    }



    /**
     *  En función de los colores encendidos, calcula un gasto en voltios y amperios
     *  Devuelve los vatios
     */
    double calculateConsumtion(int R, int G, int B){
        double v=0.0;
        double i=0.0;

        //Calculo de voltaje e intensidad si se usa el led Rojo
        if(R>0){v=v+2.1; i=(i+(0.02*R)/255); }

        //Calculo de voltaje e intensidad si se usa el led Verde
        if(G>0){v=v+3.2; i=(i+(0.02*G)/255);}

        //Calculo de voltaje e intensidad si se usa el led Azul
        if(B>0){ v=v+3.2; i=(i+(0.02*B)/255); }

        double vatios = (v*i);
        return vatios;
    }


    /**
     * Establece una hora para una fecha determinada
     * Función utilizada para facilitar consultas por fecha
     *
     * @param date
     * @param H
     * @param Min
     * @param Sec
     * @param Milli
     * @return
     */
    public Date setTime (Date date, int H, int Min, int Sec, int Milli) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, H);
        cal.set(Calendar.MINUTE, Min);
        cal.set(Calendar.SECOND, Sec);
        cal.set(Calendar.MILLISECOND, Milli);
        return cal.getTime();
    }


    public String getDateFormmated(Date date){

        String todayString="";
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);

        todayString += calendar.get(Calendar.YEAR) +"-";
        todayString += String.format("%02d" ,calendar.get(Calendar.MONTH))+ "-";
        todayString += String.format("%02d" ,calendar.get(Calendar.DAY_OF_MONTH))+ "T";
        todayString += String.format("%02d" ,calendar.get(Calendar.HOUR_OF_DAY))+ ":";
        //todayString += String.format("%02d" ,calendar.get(Calendar.MINUTE));
        todayString += "00";

        return todayString;

    }


    /**
     * Consulta el coste diario de los Megavatios/hora en función de los consumos
     * almacenados en la BBDD
     */
    double calculateCostDaily(final DocumentReference dr){
        double totalCost=0;

        final List<Consumos> listaConsumos = new ArrayList<>();

        Date d = new Date(); //Fecha actual
        //Log.d("Consulta precios: ", "lanzando consulta...");

        final Date today = setTime(d,0,0,0,0 ); //Inico del día de hoy
        final Date tomorrow = setTime(d, 23,59,59,999); //Fin del día de hoy

        //Log.d("Consulta precios: ", "Fechas--> "+today.toString()+" || "+tomorrow.toString());

        //Se consultan los registros del día de hoy
        dr.collection("consumos").
                whereGreaterThan("momento",today).
                whereLessThan("momento",tomorrow).
                whereEqualTo("procesado", false).
                orderBy("momento", Query.Direction.ASCENDING).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {

            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                Log.d("Sin procesar...", "Proceso consumos");
                if(task.isSuccessful()){
                    for(QueryDocumentSnapshot qs: task.getResult()){

                        Consumos c= qs.toObject(Consumos.class);
                        listaConsumos.add(c);

                        c.setProcesado(true); //Al pasarlo a la lista de precios, se marca como procesado
                        qs.getReference().set(c); //Se actualiza en firestore

                    }

                    int contador=0; //Contador de registros
                    long totalTimeSecs = 0; //Tiempo total de consumo

                    //Almacena los consumos para enviarlos a la función de consula, con la fecha en formato REE (Red electríca española
                    ArrayList<ConsumosGastos> lecturas = new ArrayList<>();

                    while(contador < (listaConsumos.size()-1)){
                        //Tiempo en segundos de un consumo determinado
                        long timeTranscurred = ((listaConsumos.get(contador+1).getMomento().getTime() - listaConsumos.get(contador).getMomento().getTime())/ 1000 );

                        //Se almacena en una variable el total de tiempo utilizado hoy
                        totalTimeSecs = totalTimeSecs + timeTranscurred;
                        //Se calculan los vatios consumidos, se obtiene una media entre el encendido y el apagado para corregir las desviaciones de intensidad automáticas
                        double mediaVatios = ((listaConsumos.get(contador+1).getVatios()+listaConsumos.get(contador).getVatios()))/2;

                        //Solo se calculan los precios cuando el estado es apagado.
                        if(!listaConsumos.get(contador+1).isEstado()) {
                            Date q_fecha= listaConsumos.get(contador+1).getMomento();
                            String q_fechaRee = getDateFormmated(listaConsumos.get(contador+1).getMomento());

                            ConsumosGastos cg = new ConsumosGastos(q_fechaRee, q_fecha, timeTranscurred, mediaVatios);
                            lecturas.add(cg);

                            //Se maracan los datos como procesados en los consumos


                        }

                        contador=contador+2; //Incremento de 2
                    }


                    //Log.d("Consulta de precios: ", "Tiempo total de hoy en segundos ------> "+ totalTimeSecs);


                    String todayAPI = getDateFormmated(today);
                    String tomorrowAPI = getDateFormmated(tomorrow);

                    String api_query = "https://apidatos.ree.es/es/datos/mercados/precios-mercados-tiempo-real?" +
                            "start_date="+todayAPI+"&" +
                            "end_date="+tomorrowAPI+"&" +
                            "time_trunc=hour";

                    //Lectura del precio
                    ConsultaPrecios cp = new ConsultaPrecios(api_query, lecturas, dr);
                    cp.execute();



                } else {
                    Log.w("Consulta precios: ", "Tarea no completada");
                    Log.w( "Fallo consulta precios ",task.getException());
                }
            }
        });

        //Obtención del precio de hoy

        return totalCost;
    }


    /**
     * Función que actualiza los gastos en Firestore mediante una lista de gastos
     * Y la refernecia al documento que corresponda (el nodo a actualizar sus gastos)
     * @param gastos
     * @param dr
     */
    public static void actualizarGastos(List<Gastos> gastos, DocumentReference dr){

        for (Gastos g:gastos) {
            //Log.d("Gastos: ", String.valueOf(g.getPrecio()));
            dr.collection("gastos").add(g).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                @Override
                public void onComplete(@NonNull Task<DocumentReference> task) {
                    if(task.getException() !=null || !task.isSuccessful()){
                        Log.d("Gastos: ", "Fallo al actualizar los gastos en Firestore");
                    }
                }
            });
        }

        Log.d("Gastos", "Gastos actualizados en Firestore");
    }





    ////////////////////////////////////////////////////////////
    ////////// Funciones UART //////////////////////////////////
    ////////////////////////////////////////////////////////////

    /**
     * Devuelve el listado de comunicaciones UART disponibles
     * @return
     */
    public static  List<String> UART_disponibles() {
        return PeripheralManager.getInstance().getUartDeviceList();
    }



    public void crearUART(String Nombre){
        // Attempt to access the UART device
        try {
            PeripheralManager manager = PeripheralManager.getInstance();
            this.uart = manager.openUartDevice(Nombre);
            this.configurarUART();
        } catch (IOException e) {
            Log.w(TAG, "Unable to access UART device", e);
        }

    }

    public void configurarUART() throws IOException {
        // Configure the UART port
        this.uart.setBaudrate(9600);
        this.uart.setDataSize(8);
        this.uart.setParity(UartDevice.PARITY_NONE);
        this.uart.setStopBits(1);
    }



    //Escribe en la UART
    public void escribirUART(String s) {
        try {
            int escritos = this.uart.write(s.getBytes(), s.length());
            getNodesFromFirestore();
            Log.d(TAG, escritos+" bytes escritos en UART");
        } catch (IOException e) {
            Log.w(TAG, "Error al escribir en UART", e);
        }
    }



    //Lee por la UART
    public String leer() {
        String s = "";
        int len;
        final int maxCount = 300; // Máximo de datos leídos cada vez
        byte[] buffer = new byte[maxCount];
        try {
            do {
                len = this.uart.read(buffer, buffer.length);
                for (int i=0; i<len; i++) {
                    s += (char)buffer[i];
                    //Log.d("Bytes: ", s);
                }
                Log.d(TAG, "Recibido de Arduino --> "+s);
                //updateRealTimeView(s);

            } while(len>0);
        } catch (IOException e) {
            Log.w(TAG, "Error al leer de UART", e);
        }
        return s;
    }

    public JSONObject leerJSON(){

        boolean isJSON = false;

        String s = "";
        int len;
        final int maxCount = 1000; //Máximo de datos leídos cada vez
        byte[] buffer = new byte[maxCount];

        JSONObject jo = null;

        while(!isJSON) {
            try {
                do {
                    len = this.uart.read(buffer, buffer.length);
                    for (int i = 0; i < len; i++) {
                        s += (char) buffer[i];
                       //Log.d("Bytes: ", s);
                    }
                } while (len > 0);

                if(s.length()<1){break;} //Si no se ha podido leer nada, se sale del loop

                jo = new JSONObject(s);
                isJSON=true;

            } catch (IOException e) {
                Log.w(TAG, "Error al leer de UART", e);
            } catch (JSONException e) {
                isJSON = false;
                Log.w(TAG, "Fallo al convertir a JSON el string leido: ");
            }
            Log.d(TAG, "Recibido de Arduino --> "+s);

        }

        return  jo;
    }


    public void unregisterUartCallbacks(){
        this.uart.unregisterUartDeviceCallback(uartCallback);
    }

    public void UartClose(){
        if (this.uart != null) {
            try {
                this.uart.close();
                this.uart = null;
            } catch (IOException e) {
                Log.w(TAG, "Unable to close UART device", e);
            }
        }
    }

    public UartDevice getUart() {
        return uart;
    }

    public void setUart(UartDevice uart) {
        this.uart = uart;
    }

    public ArrayList<String> getUartBuffer() {
        return UartBuffer;
    }

    public void setUartBuffer(ArrayList<String> uartBuffer) {
        UartBuffer = uartBuffer;
    }

    public String getUartReadData() {
        return UartReadData;
    }

    public void setUartReadData(String uartReadData) {
        UartReadData = uartReadData;
    }

    public List<DocumentReference> getNodos_fireStore() {
        return Nodos_fireStore;
    }

    public void setNodos_fireStore(List<DocumentReference> nodos_fireStore) {
        Nodos_fireStore = nodos_fireStore;
    }

    public UartDeviceCallback getUartCallback() {
        return uartCallback;
    }

    public void setUartCallback(UartDeviceCallback uartCallback) {
        this.uartCallback = uartCallback;
    }

    public FirebaseFirestore getFb() {
        return fb;
    }

    public void setFb(FirebaseFirestore fb) {
        this.fb = fb;
    }

    public List<Nodo> getNodosLista() {
        return NodosLista;
    }

    public void setNodosLista(List<Nodo> nodosLista) {
        NodosLista = nodosLista;
    }
}
