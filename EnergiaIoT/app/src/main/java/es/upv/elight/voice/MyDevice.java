package es.upv.elight.voice;

public class MyDevice {

    public static final String MODEL_ID = "elight-0001-GTI"; //Modelo registrado
    public static final String INSTANCE_ID = "001"; //Dispositivo de desarrollo
    public static final String LANGUAGE_CODE = "en-US"; //Idioma del asistente

    //Botones en función del dispositivo usados
    public static final String GPIO_LED = "BCM25";
    public static final String GPIO_BUTTON = "BCM23";

}
