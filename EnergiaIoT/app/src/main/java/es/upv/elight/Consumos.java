package es.upv.elight;

import java.util.Date;

public class Consumos {

    private double vatios;
    private Date momento;
    private boolean estado;
    private boolean procesado;

    public Consumos(){

    }

    public Consumos(double vatios, Date momento) {
        this.vatios = vatios;
        this.momento = momento;
    }

    public Consumos(double vatios, Date momento, boolean estado) {
        this.vatios = vatios;
        this.momento = momento;
        this.estado = estado;
    }


    public double getVatios() {
        return vatios;
    }

    public void setVatios(double vatios) {
        this.vatios = vatios;
    }

    public Date getMomento() {
        return momento;
    }

    public void setMomento(Date momento) {
        this.momento = momento;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public boolean isProcesado() {
        return procesado;
    }

    public void setProcesado(boolean procesado) {
        this.procesado = procesado;
    }
}
