package es.upv.elight;

import java.util.Date;

public class Gastos {

    private Date momento;
    private double mediaVatios;
    private double precio;

    public Gastos(){

    }

    public Gastos(Date momento, double mediaVatios, double precio) {
        this.momento = momento;
        this.mediaVatios = mediaVatios;
        this.precio = precio;
    }

    public Date getMomento() {
        return momento;
    }

    public void setMomento(Date momento) {
        this.momento = momento;
    }

    public double getMediaVatios() {
        return mediaVatios;
    }

    public void setMediaVatios(double mediaVatios) {
        this.mediaVatios = mediaVatios;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
