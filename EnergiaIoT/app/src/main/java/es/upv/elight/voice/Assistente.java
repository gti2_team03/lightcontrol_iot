package es.upv.elight.voice;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorSpace;
import android.media.AudioDeviceInfo;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.Constraints;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.things.contrib.driver.button.Button;
import com.google.android.things.contrib.driver.voicehat.Max98357A;
import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.PeripheralManager;
import com.google.assistant.embedded.v1alpha2.AssistConfig;
import com.google.assistant.embedded.v1alpha2.AssistRequest;
import com.google.assistant.embedded.v1alpha2.AssistResponse;
import com.google.assistant.embedded.v1alpha2.AudioOutConfig;
import com.google.assistant.embedded.v1alpha2.DeviceConfig;
import com.google.assistant.embedded.v1alpha2.DialogStateIn;
import com.google.assistant.embedded.v1alpha2.EmbeddedAssistantGrpc;
import com.google.assistant.embedded.v1alpha2.SpeechRecognitionResult;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.protobuf.ByteString;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import es.upv.elight.Communications;
import es.upv.elight.Nodo;
import es.upv.iilumination_2.R;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.auth.MoreCallCredentials;
import io.grpc.stub.StreamObserver;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static es.upv.elight.voice.AudioConfig.ASSISTANT_AUDIO_REQUEST_CONFIG;
import static es.upv.elight.voice.AudioConfig.ASSISTANT_AUDIO_RESPONSE_CONFIG;
import static es.upv.elight.voice.AudioConfig.AUDIO_FORMAT_IN_MONO;
import static es.upv.elight.voice.AudioConfig.AUDIO_FORMAT_OUT_MONO;
import static es.upv.elight.voice.AudioConfig.AUDIO_FORMAT_STEREO;
import static es.upv.elight.voice.AudioConfig.ENCODING_OUTPUT;
import static es.upv.elight.voice.AudioConfig.SAMPLE_BLOCK_SIZE;
import static es.upv.elight.voice.AudioConfig.SAMPLE_RATE;

public class Assistente implements Button.OnButtonEventListener{

    private Handler mMainHandler;
    private int mOutputBufferSize; //Espacio del buffer de salida

    // Audio playback and recording objects.
    private AudioTrack mAudioTrack;
    private AudioRecord mAudioRecord;

    // Audio routing configuration: use default routing.
    private AudioDeviceInfo mAudioInputDevice;
    private AudioDeviceInfo mAudioOutputDevice;

    // Hardware peripherals.
    private Button mButton;
    private Gpio mLed;
    private Max98357A mDac;
    private Handler mLedHandler = new Handler(Looper.getMainLooper());



    // Assistant Thread and Runnables implementing the push-to-talk functionality.
    private ByteString mConversationState = null;
    private HandlerThread mAssistantThread;
    private Handler mAssistantHandler;
    private ArrayList<ByteBuffer> mAssistantResponses = new ArrayList<>();
    private static int mVolumePercentage = 100;


    // Google Assistant API constants.
    private static final String ASSISTANT_ENDPOINT = "embeddedassistant.googleapis.com";
    private static final int BUTTON_DEBOUNCE_DELAY_MS = 20;

    private Context context;
    private Communications com;

    public Assistente(Context context, Communications com){
        this.context = context;
        this.com = com;

        AssistenteStart();
    }



    public void AssistenteStart(){

        this.mAssistantThread = new HandlerThread("assistantThread");
        this.mAssistantThread.start();
        this.mAssistantHandler = new Handler(mAssistantThread.getLooper());


        try {
            mButton = new Button(MyDevice.GPIO_BUTTON, Button.LogicState.PRESSED_WHEN_LOW); //Instancia del botón y el pulso por defecto
            mLed = PeripheralManager.getInstance().openGpio(MyDevice.GPIO_LED); //Instancia del led

            mButton.setDebounceDelay(BUTTON_DEBOUNCE_DELAY_MS);
            mButton.setOnButtonEventListener(this);

            mLed.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
            mLed.setActiveType(Gpio.ACTIVE_HIGH);
        } catch (IOException e) {
            Log.e(Constraints.TAG, "error configuring peripherals:", e);
            return;
        }


        AudioManager manager = (AudioManager)this.context.getSystemService(Context.AUDIO_SERVICE);
        int maxVolume = manager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        Log.i("Info", "setting volume to: " + maxVolume);
        manager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, 0);
        mOutputBufferSize = AudioTrack.getMinBufferSize(AUDIO_FORMAT_OUT_MONO.getSampleRate(),
                AUDIO_FORMAT_OUT_MONO.getChannelMask(),
                AUDIO_FORMAT_OUT_MONO.getEncoding());
        mAudioTrack = new AudioTrack.Builder()
                .setAudioFormat(AUDIO_FORMAT_OUT_MONO)
                .setBufferSizeInBytes(mOutputBufferSize)
                .build();
        mAudioTrack.play();
        int inputBufferSize = AudioRecord.getMinBufferSize(AUDIO_FORMAT_STEREO.getSampleRate(),
                AUDIO_FORMAT_STEREO.getChannelMask(),
                AUDIO_FORMAT_STEREO.getEncoding());
        mAudioRecord = new AudioRecord.Builder()
                .setAudioSource(MediaRecorder.AudioSource.VOICE_RECOGNITION)
                .setAudioFormat(AUDIO_FORMAT_IN_MONO)
                .setBufferSizeInBytes(inputBufferSize)
                .build();

        ManagedChannel channel = ManagedChannelBuilder.forTarget(ASSISTANT_ENDPOINT).build();
        try {
            mAssistantService = EmbeddedAssistantGrpc.newStub(channel)
                    .withCallCredentials(MoreCallCredentials.from(
                            Credentials.fromResource(context, R.raw.credentials)
                    ));
        } catch (IOException| JSONException e) {
            Log.e("Error", "error creating assistant service:", e);
        }





    }


    /**
     * Encuentra el tipo de dispositivo de audio utilizado por el HW
     * @param deviceFlag
     * @param deviceType
     * @return
     */
    private AudioDeviceInfo findAudioDevice(int deviceFlag, int deviceType) {
        AudioManager manager = (AudioManager) this.context.getSystemService(Context.AUDIO_SERVICE);
        AudioDeviceInfo[] adis = manager.getDevices(deviceFlag);
        for (AudioDeviceInfo adi : adis) {
            if (adi.getType() == deviceType) {
                return adi;
            }
        }
        return null;
    }




    // gRPC client and stream observers.
    private EmbeddedAssistantGrpc.EmbeddedAssistantStub mAssistantService;
    private StreamObserver<AssistRequest> mAssistantRequestObserver;


    private StreamObserver<AssistResponse> mAssistantResponseObserver =
            new StreamObserver<AssistResponse>() {
                @Override
                public void onNext(AssistResponse value) {


                    if (value.getEventType() != null) {
                        //Log.d("Debug", "converse response event: " + value.getEventType());
                    }
                    if (value.getSpeechResultsList() != null && value.getSpeechResultsList().size() > 0) {
                        for (SpeechRecognitionResult result : value.getSpeechResultsList()) {
                            final String spokenRequestText = result.getTranscript();
                            if (!spokenRequestText.isEmpty()) {
                                Log.i("Info", "assistant request text: " + spokenRequestText);
                                //mMainHandler.post(() -> mAssistantRequestsAdapter.add(spokenRequestText));
                            }
                        }
                    }
                    if (value.getDialogStateOut() != null) {
                        //mConversationState = value.getDialogStateOut().getConversationState();
                        int volume = value.getDialogStateOut().getVolumePercentage();
                        if (volume > 0) {
                            mVolumePercentage = volume;
                            Log.i(TAG, "assistant volume changed: " + mVolumePercentage);
                            mAudioTrack.setVolume(AudioTrack.getMaxVolume() *
                                    mVolumePercentage / 100.0f);
                        }
                        mConversationState = value.getDialogStateOut().getConversationState();
                    }
                    if (value.getAudioOut() != null) {
                        final ByteBuffer audioData =
                                ByteBuffer.wrap(value.getAudioOut().getAudioData().toByteArray());
                        //Log.d("Debug", "converse audio size: " + audioData.remaining());
                        mAssistantResponses.add(audioData);
                    }

                    //Si es un comando personalizado
                    if (value.getDeviceAction() != null &&
                            !value.getDeviceAction().getDeviceRequestJson().isEmpty()) {
                        // Iterate through JSON object
                        try {
                            JSONObject deviceAction =
                                    new JSONObject(value.getDeviceAction().getDeviceRequestJson());

                            Log.d(TAG, "JSON: "+deviceAction.toString());

                            JSONArray inputs = deviceAction.getJSONArray("inputs");
                            for (int i = 0; i < inputs.length(); i++) {
                                if (inputs.getJSONObject(i).getString("intent")
                                        .equals("action.devices.EXECUTE")) {
                                    JSONArray commands = inputs.getJSONObject(i)
                                            .getJSONObject("payload")
                                            .getJSONArray("commands");
                                    for (int j = 0; j < commands.length(); j++) {
                                        JSONArray execution = commands.getJSONObject(j)
                                                .getJSONArray("execution");
                                        for (int k = 0; k < execution.length(); k++) {
                                            String command = execution.getJSONObject(k)
                                                    .getString("command");
                                            JSONObject params = execution.getJSONObject(k)
                                                    .optJSONObject("params");
                                            handleDeviceAction(command, params); //Se procesa el comando
                                        }
                                    }
                                }
                            }
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onError(Throwable t) {
                   // t.printStackTrace();
                    Log.e("Error", "Error de conversion:", t);
                    this.onCompleted();
                }

                @Override
                public void onCompleted() {
                    procesaAudioCompletado();
                }


            };


    /**
     * Función para procesar los comandos personalizados
     * @param command
     * @param params
     * @throws JSONException
     * @throws IOException
     */
 /*   public void handleDeviceAction(String command, JSONObject params)
            throws JSONException, IOException {
        if (command.equals("action.devices.traits.OnOff")) {
            mLed.setValue(params.getBoolean("on"));
        }
    }
*/

    public void handleDeviceAction(String command, JSONObject params) throws JSONException, IOException {
        mLedHandler.removeCallbacksAndMessages(null);
        //Si se detecta el encendido de luces
        if(command.equals("com.example.commands.TurningON")){
            String place = params.getString("place");

            this.com.getFb().collection("nodos").whereEqualTo("nombre", place).get().addOnCompleteListener(task -> {
                if(task.isSuccessful()){ //Si se completa con éxito
                    //Envía a Firestore

                    for(QueryDocumentSnapshot qs: task.getResult()){
                        Nodo n = qs.toObject(Nodo.class);

                        //Enciende las luces
                        n.setAutomatico(false); n.setEstado(true);n.setUpdatedViaMobile(true);

                        DocumentReference dr = qs.getReference();
                        dr.set(n).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Log.d("Actualizado", "Actualizado con exito");
                                    Communications.last_updates.put(n.getMAC(), n.getMomento());
                                }
                                else {
                                    task.getException().printStackTrace();
                                    Log.d("No Actualizado", "No actualizado");

                                }
                            }
                        });
                    }

                }else{
                    Log.e(TAG, "Fallo en la actualización por voz");
                }
            });
        }

        //Si se detecta el apagado de luces
        if(command.equals("com.example.commands.TurningOFF")){

            String place = params.getString("place");

            this.com.getFb().collection("nodos").whereEqualTo("nombre", place).get().addOnCompleteListener(task -> {
                if(task.isSuccessful()){ //Si se completa con éxito
                    //Envía a Firestore

                    for(QueryDocumentSnapshot qs: task.getResult()){
                        Nodo n = qs.toObject(Nodo.class);

                        Log.d("Actualizando: ", "Nodo: "+n.toString());

                        //Apaga las luces
                        n.setAutomatico(false); n.setEstado(false);
                        n.setUpdatedViaMobile(true);

                        DocumentReference dr = qs.getReference();
                        dr.set(n).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Log.d("Actualizado", "Actualizado con exito");
                                    Communications.last_updates.put(n.getMAC(), n.getMomento());
                                }
                                else {
                                    task.getException().printStackTrace();
                                    Log.d("No Actualizado", "No actualizado");

                                }

                            }
                        });
                    }

                }else{
                    Log.e(TAG, "Fallo en la actualización por voz");
                }
            });

        }



        //Si se detecta un cambio de color
        if(command.equals("com.example.commands.ChangeColor")){

            String place = params.getString("place");
            String color = params.getString("color");

            Log.d("Cambio color", "Color: "+color);

            //String queryPlace = place.substring(0,"colors");
            this.com.getFb().collection("nodos").whereEqualTo("nombre", place).get().addOnCompleteListener(task -> {
                if(task.isSuccessful()){ //Si se completa con éxito
                    //Envía a Firestore

                    Log.d("Cambio color", "Tarea completada");

                    for(QueryDocumentSnapshot qs: task.getResult()){
                        Nodo n = qs.toObject(Nodo.class);

                        //Log.d("Color", color);
                        if(color.compareTo("White")!=0){
                            ColorSpace sRgb = ColorSpace.get(ColorSpace.Named.SRGB);
                            try {
                            Color m_color = Color.valueOf(Color.parseColor(color));
                            m_color = m_color.convert(sRgb);

                            List<Integer> rgb_color = new ArrayList<>();
                            rgb_color.add((int)(m_color.red()*255));
                            rgb_color.add((int)(m_color.green()*255));
                            rgb_color.add((int)(m_color.blue()*255));

                            n.setColor(rgb_color);
                            n.setColor_custom(true);
                            } catch (IllegalArgumentException ex){
                                Log.w("Color desconocido", "Poner color por defecto");
                                List<Integer> rgb_color = new ArrayList<>();
                                rgb_color.add(20);rgb_color.add(20);rgb_color.add(20);
                                n.setColor(rgb_color);
                                n.setColor_custom(false);
                            }
                        } else {
                            n.setColor_custom(false);
                        }

                        n.setUpdatedViaMobile(true);


                        DocumentReference dr = qs.getReference();
                        dr.set(n).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Log.d("Actualizado", "Actualizado color con exito");
                                    Communications.last_updates.put(n.getMAC(), n.getMomento());
                                }
                                else {
                                    task.getException().printStackTrace();
                                    Log.d("No Actualizado", "No actualizado el color");

                                }

                            }
                        });
                    }

                }else{
                    Log.e(TAG, "Fallo en la actualización por voz");
                }
            });

        }


        //Si se detecta un cambio de color
        if(command.equals("com.example.commands.TurningAUTO")){

            String place = params.getString("place");

            this.com.getFb().collection("nodos").whereEqualTo("nombre", place).get().addOnCompleteListener(task -> {
                if(task.isSuccessful()){ //Si se completa con éxito
                    //Envía a Firestore

                    for(QueryDocumentSnapshot qs: task.getResult()){
                        Nodo n = qs.toObject(Nodo.class);

                        n.setEstado(false);
                        n.setAutomatico(true);
                        n.setUpdatedViaMobile(true);

                        DocumentReference dr = qs.getReference();
                        dr.set(n).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Log.d("Actualizado", "Actualizado con exito");
                                    Communications.last_updates.put(n.getMAC(), n.getMomento());
                                }
                                else {
                                    task.getException().printStackTrace();
                                    Log.d("No Actualizado", "No actualizado");

                                }

                            }
                        });
                    }

                }else{
                    Log.e(TAG, "Fallo en la actualización por voz");
                }
            });

        }



    }


    /**
     * Procesamiento de audio al completar una solicitud
     */
    public void procesaAudioCompletado(){

            mAudioTrack = new AudioTrack.Builder()
                    .setAudioFormat(AUDIO_FORMAT_OUT_MONO)
                    .setBufferSizeInBytes(mOutputBufferSize)
                    .setTransferMode(AudioTrack.MODE_STREAM)
                    .build();
            if (mAudioOutputDevice != null) {
                mAudioTrack.setPreferredDevice(mAudioOutputDevice);
            }
            mAudioTrack.play();
            if (mDac != null) {
                try {
                    mDac.setSdMode(Max98357A.SD_MODE_LEFT);
                } catch (IOException e) {
                    Log.e("Error", "unable to modify dac trigger", e);
                }
            }
            for (ByteBuffer audioData : mAssistantResponses) {
                final ByteBuffer buf = audioData;
                Log.d("Debug", "Playing a bit of audio");
                mAudioTrack.write(buf, buf.remaining(),
                        AudioTrack.WRITE_BLOCKING);
            }


            mAssistantResponses.clear();
            mAudioTrack.stop();
            if (mDac != null) {
                try {
                    mDac.setSdMode(Max98357A.SD_MODE_SHUTDOWN);
                } catch (IOException e) {
                    Log.e("Error", "unable to modify dac trigger", e);
                }
            }

            Log.i("Info", "assistant response finished");
            if (mLed != null) {
                try {
                    mLed.setValue(false);
                } catch (IOException e) {
                    Log.e("Error", "error turning off LED:", e);
                }
            }
        }



        ////////////////////////////
        ////// Hilos de ejecución //
        ////////////////////////////


    /**
     * Hilo para iniciar el asistente
     * Toma una muestra de audio, y crea un hilo de streaming
     */
    private Runnable mStartAssistantRequest = new Runnable() {
        @Override
        public void run() {
            Log.i("Info", "starting assistant request");
            mAudioRecord.startRecording();
            mAssistantRequestObserver = mAssistantService.assist(mAssistantResponseObserver);
            AssistConfig.Builder converseConfigBuilder = AssistConfig.newBuilder()
                    .setAudioInConfig(ASSISTANT_AUDIO_REQUEST_CONFIG)
                    //.setAudioOutConfig(ASSISTANT_AUDIO_RESPONSE_CONFIG)
                    .setAudioOutConfig(AudioOutConfig.newBuilder()
                            .setEncoding(ENCODING_OUTPUT)
                            .setSampleRateHertz(SAMPLE_RATE)
                            .setVolumePercentage(mVolumePercentage)
                            .build())
                    .setDeviceConfig(DeviceConfig.newBuilder()
                            .setDeviceModelId(MyDevice.MODEL_ID)
                            .setDeviceId(MyDevice.INSTANCE_ID)
                            .build());
            DialogStateIn.Builder dialogStateInBuilder = DialogStateIn.newBuilder()
                    .setLanguageCode(MyDevice.LANGUAGE_CODE);
            if (mConversationState != null) {
                dialogStateInBuilder.setConversationState(mConversationState);
            }
            converseConfigBuilder.setDialogStateIn(dialogStateInBuilder.build());
            mAssistantRequestObserver.onNext(
                    AssistRequest.newBuilder()
                            .setConfig(converseConfigBuilder.build())
                            .build());


            mAssistantHandler.post(mStreamAssistantRequest);
        }
    };

    /**
     * Hilo de streaming de una audio, se Manda una solicitud con la grabación al asistente
     */
    private Runnable mStreamAssistantRequest = new Runnable() {
        @Override
        public void run() {
            ByteBuffer audioData = ByteBuffer.allocateDirect(SAMPLE_BLOCK_SIZE);
            if (mAudioInputDevice != null) {
                mAudioRecord.setPreferredDevice(mAudioInputDevice);
            }
            int result =
                    mAudioRecord.read(audioData, audioData.capacity(), AudioRecord.READ_BLOCKING);

                Log.d("Debug", "streaming ConverseRequest: " + result);

                mAssistantRequestObserver.onNext(AssistRequest.newBuilder()
                        .setAudioIn(ByteString.copyFrom(audioData))
                        .build());
                mAssistantHandler.post(mStreamAssistantRequest);

            if (result < 0) {
                Log.e("Error", "error reading from audio stream:" + result);
                return;
            }

        }
    };


    /**
     * Petición de detención del asistente
     * Detiene la grabación y ejecuta el audio almacenado en la pista de respuesta
     */
    private Runnable mStopAssistantRequest = new Runnable() {
        @Override
        public void run() {
            Log.i("Info", "ending assistant request");
            mAssistantHandler.removeCallbacks(mStreamAssistantRequest);
            if (mAssistantRequestObserver != null) {
                mAssistantRequestObserver.onCompleted();
                mAssistantRequestObserver = null;
            }
            mAudioRecord.stop();
            mAudioTrack.play();
        }
    };


    // Almacena las peticiones realizadas
    private ArrayList<String> mAssistantRequests = new ArrayList<>();
    private ArrayAdapter<String> mAssistantRequestsAdapter;




    /**
     * Libera los recursos ocupados por el servicio de
     * asistencia de voz
     */
    public void cierreAsistente(){
        Log.i("Info", "destroying assistant demo");
        if (mAudioRecord != null) {
            mAudioRecord.stop();
            mAudioRecord = null;
        }
        if (mAudioTrack != null) {
            mAudioTrack.stop();
            mAudioTrack = null;
        }
        if (mLed != null) {
            try {
                mLed.close();
            } catch (IOException e) {
                Log.w("Warning", "error closing LED", e);
            }
            mLed = null;
        }
        if (mButton != null) {
            try {
                mButton.close();
            } catch (IOException e) {
                Log.w("Warning", "error closing button", e);
            }
            mButton = null;
        }
        if (mDac != null) {
            try {
                mDac.close();
            } catch (IOException e) {
                Log.w("Warning", "error closing voice hat trigger", e);
            }
            mDac = null;
        }
        mAssistantHandler.post(() -> mAssistantHandler.removeCallbacks(mStreamAssistantRequest));
        mAssistantThread.quitSafely();
    }





    @Override
    public void onButtonEvent(Button button, boolean pressed) {

        try {
            if (this.mLed != null) {
                this.mLed.setValue(pressed);
            }
        } catch (IOException e) {
            Log.d(TAG, "error toggling LED:", e);
        }
        if (pressed) {
            this.mAssistantHandler.post(mStartAssistantRequest);
        } else {

            this.mAssistantHandler.post(mStopAssistantRequest);
        }
    }




}


