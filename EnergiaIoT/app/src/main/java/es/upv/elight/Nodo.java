package es.upv.elight;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Nodo {

    private String MAC; private String nombre;
    private boolean estado; private Date momento;

    private boolean automatico, intensidad_auto, color_custom;
    private List<Integer> color = new ArrayList<>(3);
    private String userId;

    private boolean updatedViaMobile = false;


    public Nodo(){

    }

    public Nodo(String MAC){
        this.MAC = MAC;
    }


    public Nodo(String MAC, String nombre, boolean estado) {
       this.MAC = MAC;
       this.nombre = nombre;
       this.estado = false;


    }

    public Nodo(String MAC, String nombre, boolean estado, boolean automatico) {
        this.MAC = MAC;
        this.nombre = nombre;
        this.automatico = automatico;
    }



    public boolean isIntensidad_auto() {
        return intensidad_auto;
    }

    public void setIntensidad_auto(boolean intensidad_auto) {
        this.intensidad_auto = intensidad_auto;
    }

    public boolean isColor_custom() {
        return color_custom;
    }

    public void setColor_custom(boolean color_custom) {
        this.color_custom = color_custom;
    }


    public List<Integer> getColor() {
        return color;
    }

    public void setColor(List<Integer> color) {
        this.color = color;
    }

    public String getMAC() {
        return MAC;
    }

    public void setMAC(String MAC) {
        this.MAC = MAC;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Date getMomento() {
        return momento;
    }

    public void setMomento(Date momento) {
        this.momento = momento;
    }

    public boolean isAutomatico() {
        return automatico;
    }

    public void setAutomatico(boolean automatico) {
        this.automatico = automatico;
    }

    public boolean isUpdatedViaMobile() {
        return updatedViaMobile;
    }

    public void setUpdatedViaMobile(boolean updatedViaMobile) {
        this.updatedViaMobile = updatedViaMobile;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
