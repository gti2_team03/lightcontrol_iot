package es.upv.elight;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.Image;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;

import es.upv.iilumination_2.R;
import top.defaults.colorpicker.ColorPickerPopup;

public class NodoAdapter extends RecyclerView.Adapter<NodoAdapter.NodoViewHolder>{

    private List<Nodo> items;
    public Context context;


    public static class NodoViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item

        public TextView nombre;
        public TextView mac;
        public ImageView estado;
        public ImageView selector;
        public ImageView nodo_auto;

        public Switch intensidad;


        public NodoViewHolder(View v) {
            super(v);
            nombre = (TextView) v.findViewById(R.id.nodo_nombre);
            mac = (TextView) v.findViewById(R.id.nodo_mac);
            estado =(ImageView) v.findViewById(R.id.nodo_imagen);
            selector= (ImageView) v.findViewById(R.id.nodo_color_selector);
            nodo_auto= (ImageView) v.findViewById(R.id.nodo_auto);
            intensidad= (Switch) v.findViewById(R.id.nodo_autoIntensity);


        }
    }

    public NodoAdapter(List<Nodo> items, Context ctx) {
        this.items = items;
        this.context = ctx;
    }



    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public NodoViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card, viewGroup, false);
        return new NodoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NodoViewHolder viewHolder, int i) {

        int img_color_filter = context.getResources().getColor(R.color.colorAccent_2, context.getTheme());

        viewHolder.nombre.setText(items.get(i).getNombre());
        viewHolder.mac.setText(items.get(i).getMAC());


        viewHolder.nodo_auto.setImageResource(R.drawable.automatico);
        viewHolder.nodo_auto.setColorFilter(null);
        viewHolder.nodo_auto.setBackgroundColor(context.getResources().getColor(R.color.Blanco, context.getTheme()));

        if(!items.get(i).isAutomatico()){
           viewHolder.nodo_auto.setColorFilter(context.getResources().getColor(R.color.Negro,context.getTheme()), PorterDuff.Mode.SRC_ATOP);

       } else { viewHolder.nodo_auto.setColorFilter(img_color_filter, PorterDuff.Mode.SRC_ATOP);}


        if (items.get(i).getEstado()) {
               viewHolder.estado.setImageResource(R.drawable.b_encendida);
           } else {
               viewHolder.estado.setImageResource(R.drawable.b_apagado);
           }

        img_color_filter = context.getResources().getColor(R.color.colorPrimary_2, context.getTheme());
        viewHolder.selector.setColorFilter(null);
        viewHolder.selector.setColorFilter(img_color_filter, PorterDuff.Mode.SRC_ATOP);
        viewHolder.selector.setBackgroundColor(context.getResources().getColor(R.color.Blanco, context.getTheme()));


        viewHolder.intensidad.setChecked(items.get(i).isIntensidad_auto());


        //Listeners
       setListeners(items.get(i),
                viewHolder.estado,
                viewHolder.selector,
                viewHolder.nombre,
                viewHolder.nodo_auto,
                viewHolder.intensidad);

    }


    /**
     * Eventos de los controles de cada item de la lista
     * @param n
     * @param lightControl
     * @param colorControl
     * @param editNameControl
     */
    public void setListeners(Nodo n, ImageView lightControl, ImageView colorControl, TextView editNameControl, ImageView autoControl, Switch intensidadAutoControl){


        //Control para controlar el encendido/apagado de los nodos
        lightControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(n.getEstado()){
                    n.setEstado(false);
                    lightControl.setImageResource(R.drawable.b_apagado);
                } else{
                    n.setEstado(true);
                    lightControl.setImageResource(R.drawable.b_encendida);
                }

                autoControl.setColorFilter(null);
                autoControl.setColorFilter(context.getResources().getColor(R.color.Negro,context.getTheme()), PorterDuff.Mode.SRC_ATOP);

                Communications.setNodeEstado(n.getMAC(), n.getEstado());

             }
            }

        );


        //Controla el estado automatico de la iluminacion
        autoControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean auto = false;
                if(n.isAutomatico()){auto = false;}
                else {
                    auto = true;
                }
                n.setAutomatico(auto);
                Communications.setNodeAuto(n.getMAC(), auto);

            }
        });



        //Control para actualizar el nombre de los nodos
        editNameControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder adb = new AlertDialog.Builder(context);
                adb.setTitle("Modify "+editNameControl.getText()+" name");
                EditText input = new EditText(context);
                input.setInputType(InputType.TYPE_CLASS_TEXT);

                adb.setView(input);


                adb.setPositiveButton("OK", (dialog, which) -> {
                    editNameControl.setText(input.getText().toString());
                    Communications.setNodeName(n.getMAC(), input.getText().toString());
                });
                adb.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

                adb.show();

            }
        });



        
        colorControl.setOnClickListener(view -> new ColorPickerPopup.Builder(context)
                .initialColor(0xffffffff) // Set initial color
                .enableBrightness(true) // Enable brightness slider or not
                .enableAlpha(false) // Enable alpha slider or not
                .okTitle(context.getResources().getString(R.string.color_select))
                .cancelTitle(context.getResources().getString(R.string.cancel))
                .showIndicator(true)
                .showValue(true)
                .build()
                .show(colorControl, new ColorPickerPopup.ColorPickerObserver() {
                    @Override
                    public void onColorPicked(int color) {
                        //colorControl.setColorFilter(color,PorterDuff.Mode.SRC_ATOP);
                        Log.d("Color seleccionado", ""+Color.valueOf(color).red());

                        List<Integer> rgb = new ArrayList<>();
                        rgb.add((int)(Color.valueOf(color).red()*255));
                        rgb.add((int)(Color.valueOf(color).green()*255));
                        rgb.add((int)(Color.valueOf(color).blue()*255));


                        Communications.setNodeCustomColor(n.getMAC(),rgb);
                    }

                }));

        intensidadAutoControl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Communications.setNodeIntensityAuto(n.getMAC(),b);
            }
        });


    }




}
