package es.upv.elight;

import java.util.Date;

/**
 * Clase utilizada para el intercambio de información entre el consumo
 * y los gastos para realizar consultas a la API de REE
 */
public class ConsumosGastos {

    private String fechaREE;
    private Date fecha;

    private long tiempo; //Tiempo para calcular el precio
    private double mediaVatios; //Media de vatios consmidos en el tiempo transcurrido



    public ConsumosGastos(String fechaREE, Date fecha, long tiempo, double mediaVatios) {
        this.fechaREE = fechaREE;
        this.fecha = fecha;
        this.tiempo = tiempo;
        this.mediaVatios = mediaVatios;
    }


    public String getFechaREE() {
        return fechaREE;
    }

    public void setFechaREE(String fechaREE) {
        this.fechaREE = fechaREE;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public long getTiempo() {
        return tiempo;
    }

    public void setTiempo(long tiempo) {
        this.tiempo = tiempo;
    }

    public double getMediaVatios() {
        return mediaVatios;
    }

    public void setMediaVatios(double mediaVatios) {
        this.mediaVatios = mediaVatios;
    }

}
