package es.upv.elight;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioDeviceInfo;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.things.contrib.driver.button.Button;
import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.PeripheralManager;
import com.google.android.things.pio.UartDevice;
import com.google.firebase.firestore.DocumentReference;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.upv.elight.voice.Assistente;
import es.upv.elight.voice.MyDevice;
import es.upv.iilumination_2.R;
import io.grpc.stub.StreamObserver;

import static androidx.constraintlayout.widget.Constraints.TAG;

/**
 * Actividad principal ejecuta la comunicación con la UART y Wifi
 * Entre los nodos y los controla mediante el asistente de voz de google assistant
 */
public class MainActivity extends Activity  {

    public static String userId = "abc"; //ID del usuario activo

    //public static LinearLayout MainLayout;
    private Communications com;

    public  RecyclerView recycler;
    public  NodoAdapter adapter;
    public  LinearLayoutManager lManager;
    public List<Nodo> NodosLista = new ArrayList<>();


    //////////////////////////////////
    ////// Asistente de voz //////////
    //////////////////////////////////
    private Assistente assistant;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // MainLayout = findViewById(R.id.MainLayout);



        //Gestión de la UART
        Log.i(TAG, "Lista de UART disponibles: " + Communications.UART_disponibles()); //Se conecta a la UART0 del dispositivo (M5Stack TX:1 RX:3)
        try {


            RecyclerView rv = findViewById(R.id.listaEventos);
            //LinearLayoutManager llm = new LinearLayoutManager(this);
            GridLayoutManager glm = new GridLayoutManager(this, 2); //Grid de 3 columnas

            rv.setLayoutManager(glm);

            NodoAdapter na = new NodoAdapter(this.NodosLista,this);
            rv.setAdapter(na);

            this.com = new Communications("UART0", rv, na, glm, this.NodosLista);


            //Inicializa el servicio del asistente con el manejador del lazo principal de la aplicación

            Log.i(TAG, "Se inicia el assistente de control");
            this.assistant = new Assistente(this, this.com);





        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Fallo UART", "Fallo al crear la UART0");
        }



    }


    @Override
    protected void onStart() {
        super.onStart();


        ActionBar bar = getActionBar();
        ColorDrawable cd = new ColorDrawable(Integer.decode(String.valueOf(getColor(R.color.colorPrimary_2))));
        bar.setBackgroundDrawable(cd);

        this.setTitle("Control light");

        this.com.readNodes(); //Se leen los nodos existentes y se registran sus callbacks
        this.com.registerCallbacks();

    }

    @Override
    protected void onStop() {
        super.onStop();
        this.com.unregisterUartCallbacks(); //Desregistra los callback de la UART
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.com.UartClose(); //Cierra la comunicación UART en caso de destruir
        this.assistant.cierreAsistente();

    }

    public List<Nodo> getNodosLista() {
        return NodosLista;
    }

    public void setNodosLista(List<Nodo> nodosLista) {
        NodosLista = nodosLista;
    }


// Fin de la actividad principal
}
