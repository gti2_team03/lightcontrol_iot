package es.upv.elight;

import android.os.AsyncTask;
import android.util.Log;

import com.google.firebase.firestore.DocumentReference;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConsultaPrecios extends AsyncTask<String, Void, JSONObject> {

    private String url; //URL de la API de consulta
    private DocumentReference dr; //Referencia al documento de Firestore del cual se está calculando el consumo

    private ArrayList<ConsumosGastos> lecturas = new ArrayList<>(); //Lista para saber que consumos se van a calcular

    public ConsultaPrecios(String url, ArrayList<ConsumosGastos> lecturas, DocumentReference dr){
        this.url=url;
        this.lecturas = lecturas;

        this.dr = dr;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        //Bloquear Actividad en caso de necesitarlo
    }

    @Override
    protected JSONObject doInBackground(String... strings) {
        String response;

        try {
            URL url = new URL(this.url);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();


            InputStream stream = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder builder = new StringBuilder();

            String inputString;
            while ((inputString = bufferedReader.readLine()) != null) {
                builder.append(inputString);
            }

            JSONObject result = new JSONObject(builder.toString());

            //this.raw_result = result.toString();
            //Log.d("Resultado: ", this.raw_result);

            for (ConsumosGastos cg: lecturas) {

                String fechaRee = cg.getFechaREE();
                double vatios = cg.getMediaVatios();

                Date fecha = cg.getFecha();
                long tiempo = cg.getTiempo();

                JSONObject data = result.getJSONObject("data");
                JSONArray included = result.getJSONArray("included");

                JSONObject PVPC = included.getJSONObject(0);
                JSONObject atts = PVPC.getJSONObject("attributes");
                JSONArray precios = atts.getJSONArray("values");


                List<Gastos> gastos = new ArrayList<>();

                //Busqueda entre los valores la fecha en la que se consumieron X vatios
                for (int i=0; i < precios.length(); i++) {

                    String fechaAPI = precios.getJSONObject(i).getString("datetime");
                    fechaAPI = fechaAPI.substring(0, 16);

                    if(fechaAPI.contains(fechaRee)){ //A dicha fecha y hora se coge ese precio como valido
                        Gastos g = new Gastos();
                        g.setMediaVatios(vatios);
                        g.setMomento(fecha);

                        double precio = precios.getJSONObject(i).getLong("value"); //Precio MW/Hora

                        double precioVatio = precio*0.000001; //Precio en W/Hora
                        double precioVatioSegundo = precioVatio/3600; //Precio en W/Segundo

                        double consumoRealizado = tiempo*vatios; //Consumio de vatios a lo largo de la duración del encendido

                        double precioTotal = precioVatioSegundo*consumoRealizado; //Precio total

                        g.setPrecio(precioTotal);
                        gastos.add(g);

                    }
                }

                //Se procesan los precios y se suben a firestore
                Communications.actualizarGastos(gastos, dr);

            }
            
            urlConnection.disconnect();

        } catch (IOException |JSONException e){
            e.printStackTrace();

        }

        return null;
    }


    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

           //Desbloquear actividad en caso de ser necesario

    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
