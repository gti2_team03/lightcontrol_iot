package com.example.ellight;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public class Usuarios {
    public static void guardarUsuario(final FirebaseUser user) {
        Usuario usuario = new Usuario(user.getDisplayName(),user.getEmail(), user.getPhoneNumber(), "", "", "30:AE:A4:90:F2:A8");
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("usuarios").document(user.getUid()).set(usuario);
    }

    static void actualizarUsuario (final FirebaseUser user, String name, String mail, String phone, String direction, String country, String nodo){
        Usuario usuario = new Usuario (name, mail, phone, direction, country, nodo);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("usuarios").document(user.getUid()).set(usuario);
    }
}
