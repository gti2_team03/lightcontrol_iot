package com.example.ellight;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import com.example.ellight.ui.controllight.ControllightActivity;
import com.example.ellight.ui.feedback.Elegir;
import com.example.ellight.ui.modoahorro.AhorroActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.Button;
import android.view.MenuItem;
import android.widget.VideoView;

import java.net.URI;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences preferenciaNoche = PreferenceManager.getDefaultSharedPreferences(this);

        boolean modonoche = preferenciaNoche.getBoolean("modo_noche", true);

        if (modonoche) {
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.LightTheme);

        }
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Drawer funcion

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_graphics, R.id.nav_lights, R.id.nav_ahorro,
                R.id.nav_facture, R.id.nav_info, R.id.nav_perfil)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

    }



    @Override
    protected void onStart() {
        super.onStart();

        //Se carga el medio de video
        VideoView vv = (VideoView)findViewById(R.id.videoView);
        //Uri video_uri = Uri.parse("https://youtu.be/IWj2PnyiWzA");
        //vv.setVideoURI(video_uri);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_usuario) {
            Intent intent = new Intent(this, PreferenciasActivity.class);
            startActivity(intent);
        }


        if (id == R.id.menu_video) {
            Intent intent = new Intent(this, VideoActivity.class);
            startActivity(intent);
        }

        if (id == R.id.menu_tutorial) {
            Intent intent = new Intent(this, Tutorial.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);

    }

}
