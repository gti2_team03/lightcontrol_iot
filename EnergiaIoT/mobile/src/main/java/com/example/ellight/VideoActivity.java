package com.example.ellight;

import android.os.Bundle;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ellight.ui.video.VideoAdaptor;
import com.example.ellight.ui.video.Videos;

import java.util.Vector;

public class VideoActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    Vector<Videos> videos = new Vector<Videos>();
    public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        toolbar = findViewById(R.id.toolbar);

        recyclerView = findViewById(R.id.recyclerViewVideos);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        videos.add(new Videos("<iframe width=\"100%\" height=\"100%\" src=\" https://youtu.be/oaqkENDUNcQ\" frameborder=\"0\" allowfullscreen></iframe>"));
        videos.add(new Videos("<iframe width=\"100%\" height=\"100%\" src=\"https://youtu.be/ZZCj5Q3pBpY\" frameborder=\"0\" allowfullscreen></iframe>"));
        videos.add(new Videos("<iframe width=\"100%\" height=\"100%\" src=\"https://youtu.be/LKQMWO_Drmc \" frameborder=\"0\" allowfullscreen></iframe>"));
        videos.add(new Videos("<iframe width=\"100%\" height=\"100%\" src=\"https://youtu.be/VnyjeD264oE \" frameborder=\"0\" allowfullscreen></iframe>"));

        VideoAdaptor adaptadorVideos = new VideoAdaptor(videos);

        recyclerView.setAdapter(adaptadorVideos);

    }
}