package com.example.ellight;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.NonNull;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import static com.example.ellight.Usuarios.actualizarUsuario;

public class ProfileActivity extends Activity {
    Button guardarCambios;
    Button cancelarCambios;
    EditText valorNombre;
    EditText valorCorreo;
    EditText valorTelefono;
    EditText valorDireccion;
    EditText valorPais;
    String nodo;

    FirebaseUser usuario = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_usuario);

        guardarCambios = (Button) this.findViewById(R.id.btn_guardar);
        cancelarCambios  = (Button) this.findViewById(R.id.btn_cancelar);
        valorNombre  = (EditText) this.findViewById(R.id.nameText);
        valorCorreo  = (EditText) this.findViewById(R.id.mailText);
        valorTelefono = (EditText) this.findViewById(R.id.phoneText);
        valorDireccion = (EditText) this.findViewById(R.id.directionText);
        valorPais = (EditText) this.findViewById(R.id.countryText);

        //Conexion FireStore

        db.collection("usuarios").document(usuario.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>(){
            @Override public void onComplete(@NonNull Task<DocumentSnapshot> task){
                if(task.isSuccessful()) {

                    String nombreBBDD = task.getResult().getString("nombre");
                    String correoBBDD = task.getResult().getString("correo");
                    String telefonoBBDD = task.getResult().getString("telefono");
                    String direccionBBDD = task.getResult().getString("direccion");
                    String paisBBDD = task.getResult().getString("pais");
                    nodo = task.getResult().getString("nodo");

                    valorNombre.setText(nombreBBDD);
                    valorCorreo.setText(correoBBDD);
                    valorTelefono.setText(telefonoBBDD);
                    valorDireccion.setText(direccionBBDD);
                    valorPais.setText(paisBBDD);
                }
            }
        });

        guardarCambios.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                String nombre = valorNombre.getText().toString();
                String correo = valorCorreo.getText().toString();
                String telefono = valorTelefono.getText().toString();
                String direccion = valorDireccion.getText().toString();
                String pais = valorPais.getText().toString();

                actualizarUsuario(usuario, nombre, correo, telefono, direccion, pais, nodo);
                terminaOperacion();
            }
        });

        cancelarCambios.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                terminaOperacion();
            }
        });
    }

    public void terminaOperacion(){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        this.finish();
    }
}