package com.example.ellight.ui.controllight;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.example.ellight.Communications;
import com.example.ellight.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import yuku.ambilwarna.AmbilWarnaDialog;

public class LuzRecyclerAdapter extends FirestoreRecyclerAdapter<Luz, LuzRecyclerAdapter.LuzViewHolder> {

    private static final String TAG = "LuzRecyclerAdapter";

    public LuzListener LuzListener;
    public Context context;



    public LuzRecyclerAdapter(@NonNull FirestoreRecyclerOptions<Luz> options, LuzListener LuzListener, Context ctx) {
        super(options);

        this.context = ctx;
        this.LuzListener = LuzListener;
    }

    @Override
    protected void onBindViewHolder(@NonNull LuzViewHolder holder, int position, @NonNull Luz Luz) {
        /*holder.LuzTextView.setText(Luz.getNombre());
        holder.checkBox.setChecked(Luz.getEstado());
        CharSequence dateCharSeq = DateFormat.format("EEEE, MMM d, yyyy h:mm:ss a", Luz.getMomento().toDate());
        holder.dateTextView.setText(dateCharSeq);*/

        int img_color_filter = context.getResources().getColor(R.color.colorAccent, context.getTheme());

        holder.nombre.setText(Luz.getNombre());
        //holder.mac.setText(Luz.getMAC());


        holder.nodo_auto.setImageResource(R.drawable.automatico);
        holder.nodo_auto.setColorFilter(null);
        holder.nodo_auto.setBackgroundColor(context.getResources().getColor(R.color.Blanco, context.getTheme()));

        if(!Luz.isAutomatico()){
            holder.nodo_auto.setColorFilter(context.getResources().getColor(R.color.Negro,context.getTheme()), PorterDuff.Mode.SRC_ATOP);

        } else { holder.nodo_auto.setColorFilter(img_color_filter, PorterDuff.Mode.SRC_ATOP);}


        if (Luz.getEstado()) {
            holder.estado.setImageResource(R.drawable.b_encendida);
        } else {
            holder.estado.setImageResource(R.drawable.b_apagado);
        }

        img_color_filter = context.getResources().getColor(R.color.Negro, context.getTheme());
        holder.selector.setColorFilter(null);
        holder.selector.setColorFilter(img_color_filter, PorterDuff.Mode.SRC_ATOP);
        holder.selector.setBackgroundColor(context.getResources().getColor(R.color.Blanco, context.getTheme()));


        holder.intensidad.setChecked(Luz.isIntensidad_auto());


        //Listeners
        setListeners(Luz,
                holder.estado,
                holder.selector,
                holder.nombre,
                holder.nodo_auto,
                holder.intensidad);

    }



    /**
     * Eventos de los controles de cada item de la lista
     * @param luz
     * @param lightControl
     * @param colorControl
     * @param editNameControl
     */
    public void setListeners(final Luz luz, final ImageView lightControl, final ImageView colorControl, final TextView editNameControl,
                             final ImageView autoControl, final Switch intensidadAutoControl){


        final int img_color_filter = context.getResources().getColor(R.color.colorAccent, context.getTheme());

        //Control para controlar el encendido/apagado de los nodos
        lightControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(luz.getEstado()){
                    luz.setEstado(false);
                    lightControl.setImageResource(R.drawable.b_apagado);
                } else{
                    luz.setEstado(true);
                    lightControl.setImageResource(R.drawable.b_encendida);
                }

                autoControl.setColorFilter(null);
                autoControl.setColorFilter(context.getResources().getColor(R.color.Negro,context.getTheme()), PorterDuff.Mode.SRC_ATOP);
                Communications.setNodeEstado(luz.getMAC(), luz.getEstado(), context);

            }
        }
        );


        //Controla el estado automatico de la iluminacion
        autoControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean auto = false;
                autoControl.setColorFilter(null);

                if(luz.isAutomatico()){
                    autoControl.setColorFilter(context.getResources().getColor(R.color.Negro,context.getTheme()), PorterDuff.Mode.SRC_ATOP);
                    auto = false;
                }
                else {
                    autoControl.setColorFilter(img_color_filter, PorterDuff.Mode.SRC_ATOP);
                    auto = true;
                }
                luz.setAutomatico(auto);
                Communications.setNodeAuto(luz.getMAC(), auto);

            }
        });



        //Control para actualizar el nombre de los nodos
        editNameControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder adb = new AlertDialog.Builder(context);
                adb.setTitle("Modify "+editNameControl.getText()+" name");
                final EditText input = new EditText(context);
                input.setInputType(InputType.TYPE_CLASS_TEXT);

                adb.setView(input);

                adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editNameControl.setText(input.getText().toString());
                        Communications.setNodeName(luz.getMAC(), input.getText().toString());
                    }
                });


                adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                adb.show();

            }
        });



        final AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(context, 16777215, new AmbilWarnaDialog.OnAmbilWarnaListener() {
            //final EditText editText = new EditText(this);
            //editText.setText(luz.getColor().toString());
            @Override
            public void onCancel(AmbilWarnaDialog dialog) {

            }

            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
                //mDefaultColor = color;
                //String numberAsString = Integer.toString(mDefaultColor);



                String red = Integer.toHexString(Color.red(color));
                String green = Integer.toHexString(Color.green(color));
                String blue = Integer.toHexString(Color.blue(color));
                if (red.length() == 1)
                    red = "0" + red;
                if (green.length() == 1)
                    green = "0" + green;
                if (blue.length() == 1)
                    blue = "0" + blue;

                String hola = red + green + blue;

                int[] ret = new int[3];
                for (int i = 0; i < 3; i++)
                {
                    ret[i] = Integer.parseInt(hola.substring(i * 2, i * 2 + 2), 16);
                }

                String res = Arrays.toString(ret);


                //String newText = editText.getText().toString();
                String newText = res;
                ///
                newText = newText.replaceAll("\\D+"," ");
                Scanner scanner = new Scanner(newText);
                List<Integer> list = new ArrayList<Integer>();
                while (scanner.hasNextInt()) {
                    list.add(scanner.nextInt());
                }
                Communications.setNodeCustomColor(luz.getMAC(), list);
                Log.w("colormono", res );
                //mLayout.setBackgroundColor(mDefaultColor);
            }
        });


        colorControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorPicker.show();
            }
        });



        intensidadAutoControl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Communications.setNodeIntensityAuto(luz.getMAC(),b);
            }
        });


    }

    @Override
    public LuzViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.light_card, parent, false);
        return new LuzViewHolder(view);
    }

    public class LuzViewHolder extends RecyclerView.ViewHolder {

        TextView LuzTextView, dateTextView;
        CheckBox checkBox;


        public TextView nombre;
        public TextView mac;
        public ImageView estado;
        public ImageView selector;
        public ImageView nodo_auto;

        public Switch intensidad;


        public LuzViewHolder(@NonNull View itemView) {
            super(itemView);

            nombre = (TextView) itemView.findViewById(R.id.nodo_nombre);
            //mac = (TextView) itemView.findViewById(R.id.nodo_mac);
            estado =(ImageView) itemView.findViewById(R.id.nodo_imagen);
            selector= (ImageView) itemView.findViewById(R.id.nodo_color_selector);
            nodo_auto= (ImageView) itemView.findViewById(R.id.nodo_auto);
            intensidad= (Switch) itemView.findViewById(R.id.nodo_autoIntensity);

          /*  LuzTextView = itemView.findViewById(R.id.luzTextView);
            dateTextView = itemView.findViewById(R.id.dateTextView);
            checkBox = itemView.findViewById(R.id.checkBox);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    DocumentSnapshot snapshot = getSnapshots().getSnapshot(getAdapterPosition());
                    Luz Luz = getItem(getAdapterPosition());
                    if (Luz.getEstado() != isChecked) {
                        LuzListener.handleCheckChanged(isChecked, snapshot);
                    }
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DocumentSnapshot snapshot = getSnapshots().getSnapshot(getAdapterPosition());
                    LuzListener.handleEditLuz(snapshot);

                }
            }); */
        }

        public void deleteItem() {
            LuzListener.handleDeleteItem(getSnapshots().getSnapshot(getAdapterPosition()));
        }
    }

    public interface LuzListener {
        public void handleCheckChanged(boolean isChecked, DocumentSnapshot snapshot);
        public void handleEditLuz(DocumentSnapshot snapshot);
        public void handleDeleteItem(DocumentSnapshot snapshot);
    }




}
