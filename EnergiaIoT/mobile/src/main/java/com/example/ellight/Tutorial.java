package com.example.ellight;

import android.content.Intent;
import android.os.Bundle;


import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

public class Tutorial extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_app_intro);


        addSlide(AppIntroFragment.newInstance("Easily control all of your home light",
                "","Just with a single touch. Great degree of customization",
                "",R.drawable.recifoto, getResources().getColor(R.color.colorPrimaryDark),
                getResources().getColor(R.color.Negro),getResources().getColor(R.color.Negro)));
        addSlide(AppIntroFragment.newInstance("Watch over your electricity consumption",
                "","Electricity bill wont have any surprise from now on ",
                "",R.drawable.gasto, getResources().getColor(R.color.colorPrimaryDark),
                getResources().getColor(R.color.Negro),getResources().getColor(R.color.Negro)));
        addSlide(AppIntroFragment.newInstance("RealTime status notifications, voice control...",
                "","And much more waiting for the maximum efficiency at light control",
                "",R.drawable.tech, getResources().getColor(R.color.colorPrimaryDark),
                getResources().getColor(R.color.Negro),getResources().getColor(R.color.Negro)));

    }


    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Intent intent=new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        Intent intent=new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
        finish();
    }
}
