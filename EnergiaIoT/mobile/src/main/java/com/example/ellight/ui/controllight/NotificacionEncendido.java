package com.example.ellight.ui.controllight;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.example.ellight.MainActivity;
import com.example.ellight.R;

public class NotificacionEncendido extends Service {
    private NotificationManager notificationManager;
    static final String CANAL_ID = "mi_canal";
    static final int NOTIFICACION_ID = 1;
    @Override public void onCreate() {
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int idArranque) {

        String habitaciones = intent.getStringExtra("nodos");
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(
                    CANAL_ID, "Mis Notificaciones",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription("Descripcion del canal");
            notificationChannel.enableVibration(false);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder notificacion =
                new NotificationCompat.Builder(NotificacionEncendido.this, CANAL_ID)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Luces encendidas en este momento")
                        .setContentText(habitaciones)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                                R.mipmap.icono))
                        .setWhen(System.currentTimeMillis())
                        .setContentInfo("más info")
                        .setTicker("Texto en barra de estado");
        PendingIntent intentionPendiente = PendingIntent.getActivity(
                this, 0, new Intent(this, MainActivity.class), 0);
        notificacion.setContentIntent(intentionPendiente);
        startForeground(NOTIFICACION_ID, notificacion.build());
        return START_STICKY;
    }

    @Override public void onDestroy() {
        notificationManager.cancel(NOTIFICACION_ID);
    }

    @Override public IBinder onBind(Intent intention) {
        return null;
    }
}
