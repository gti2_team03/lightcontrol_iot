package com.example.ellight.ui.lights;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ellight.LoginActivity;
import com.example.ellight.R;
import com.example.ellight.ui.controllight.ControllightActivity;
import com.example.ellight.ui.controllight.Luz;
import com.example.ellight.ui.controllight.LuzRecyclerAdapter;
import com.example.ellight.ui.controllight.MyService;
import com.example.ellight.ui.controllight.NotificacionEncendido;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;
import yuku.ambilwarna.AmbilWarnaDialog;

public class LightsFragment extends Fragment implements FirebaseAuth.AuthStateListener, LuzRecyclerAdapter.LuzListener {

    private static final String TAG = "MainActivity";

    static LightsFragment instance;


    RecyclerView recyclerView;
    LuzRecyclerAdapter lucesRecyclerAdapter;
    private AppBarConfiguration mAppBarConfiguration;

    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_lights, container, false);


        recyclerView = root.findViewById(R.id.recyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(root.getContext(), DividerItemDecoration.VERTICAL));

        instance = this;

        Intent myIntent = new Intent(root.getContext(), MyService.class);
        root.getContext().startService(myIntent);


        return root;


    }



    private void addLuz(String text) {

        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Luz luz = new Luz(text, false, new Timestamp(new Date()), userId);

        FirebaseFirestore.getInstance()
                .collection("luces")
                .add(luz)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "onSuccess: Succesfully added the luz...");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "onFailure: " + e.getLocalizedMessage() );
                        Toast.makeText(getContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void startLoginActivity() {
        Intent intent = new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseAuth.getInstance().addAuthStateListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        FirebaseAuth.getInstance().removeAuthStateListener(this);
        if (lucesRecyclerAdapter != null) {
            lucesRecyclerAdapter.stopListening();
        }
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        if (firebaseAuth.getCurrentUser() == null) {
            startLoginActivity();
            return;
        }

        initRecyclerView(firebaseAuth.getCurrentUser());
    }

    private void initRecyclerView(FirebaseUser user) {

        Query query = FirebaseFirestore.getInstance()
                .collection("nodos")
                //.whereEqualTo("userId", user.getUid())
                .whereEqualTo("userId", "abc")
                .orderBy("estado", Query.Direction.DESCENDING)
                .orderBy("momento", Query.Direction.ASCENDING);

        FirestoreRecyclerOptions<Luz> options = new FirestoreRecyclerOptions.Builder<Luz>()
                .setQuery(query, Luz.class)
                .build();
        lucesRecyclerAdapter = new LuzRecyclerAdapter(options, this, getContext());
        recyclerView.setAdapter(lucesRecyclerAdapter);
        lucesRecyclerAdapter.startListening();

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);


    }

    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            if (direction == ItemTouchHelper.LEFT) {
                Toast.makeText(getContext(), "Deleting", Toast.LENGTH_SHORT).show();

                LuzRecyclerAdapter.LuzViewHolder luzViewHolder = (LuzRecyclerAdapter.LuzViewHolder) viewHolder;
                luzViewHolder.deleteItem();
            }
        }

        @Override
        public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                    .addBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent))
                    .addActionIcon(R.drawable.ic_delete_black_24dp)
                    .create()
                    .decorate();
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    };

    @Override
    public void handleCheckChanged(boolean isChecked,final DocumentSnapshot snapshot) {
        Log.d(TAG, "handleCheckChanged: " + isChecked);
        Log.d(TAG, "onSuccess: " + snapshot);
        snapshot.getReference().update("updatedViaMobile", true)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        Log.d(TAG, "onSuccess: ");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "onFailure: " + e.getLocalizedMessage());
                    }
                });
        snapshot.getReference().update("automatico", false)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        Log.d(TAG, "onSuccess: ");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "onFailure: " + e.getLocalizedMessage());
                    }
                });
        snapshot.getReference().update("estado", isChecked)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        Log.d(TAG, "onSuccess: ");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "onFailure: " + e.getLocalizedMessage());
                    }
                });

        snapshot.getReference().update("momento", new Timestamp(new Date()))
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        Log.d(TAG, "onSuccess: ");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "onFailure: " + e.getLocalizedMessage());
                    }
                });
        support();
    }

    public void support(){
        FirebaseFirestore.getInstance().collection("nodos")
                .whereEqualTo("estado", true)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                        String nombres= "";
                        String control= "";
                        WriteBatch batch = FirebaseFirestore.getInstance().batch();
                        List<DocumentSnapshot> snapshotList = queryDocumentSnapshots.getDocuments();

                        for (DocumentSnapshot snapshot : snapshotList) {
                            Luz product = snapshot.toObject(Luz.class);
                            nombres= nombres + "  " + product.getNombre();
                            control= control + product.getNombre();
                            //Log.d(TAG, "onComplete: " + product.getNombre());
                            //batch.update(snapshot.getReference(),"estado",false);
                        }
                        //Log.d(TAG, "onComplete: " + nombres);
                        Log.d(TAG, "onComplete: " + control);

                        Intent serviceIntent = new Intent(new Intent(getContext(),
                                NotificacionEncendido.class));
                        serviceIntent.putExtra("nodos", nombres);
                        getContext().startService(serviceIntent);

                        if(control==""){
                            getContext().stopService(serviceIntent);
                        }else{
                            getContext().stopService(serviceIntent);
                            getContext().startService(serviceIntent);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });


    }



    @Override
    public void handleEditLuz(final DocumentSnapshot snapshot) {

        AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(getContext(), 16777215, new AmbilWarnaDialog.OnAmbilWarnaListener() {
            final Luz luz = snapshot.toObject(Luz.class);
            //final EditText editText = new EditText(this);
            //editText.setText(luz.getColor().toString());
            @Override
            public void onCancel(AmbilWarnaDialog dialog) {

            }

            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
                //mDefaultColor = color;
                //String numberAsString = Integer.toString(mDefaultColor);



                String red = Integer.toHexString(Color.red(color));
                String green = Integer.toHexString(Color.green(color));
                String blue = Integer.toHexString(Color.blue(color));
                if (red.length() == 1)
                    red = "0" + red;
                if (green.length() == 1)
                    green = "0" + green;
                if (blue.length() == 1)
                    blue = "0" + blue;

                String hola = red + green + blue;

                int[] ret = new int[3];
                for (int i = 0; i < 3; i++)
                {
                    ret[i] = Integer.parseInt(hola.substring(i * 2, i * 2 + 2), 16);
                }

                String res = Arrays.toString(ret);


                //String newText = editText.getText().toString();
                String newText = res;
                ///
                newText = newText.replaceAll("\\D+"," ");
                Scanner scanner = new Scanner(newText);
                List<Integer> list = new ArrayList<Integer>();
                while (scanner.hasNextInt()) {
                    list.add(scanner.nextInt());
                }
                snapshot.getReference().update("color", list);
                Log.w("colormono", res );
                //mLayout.setBackgroundColor(mDefaultColor);
            }
        });
        colorPicker.show();
    }

    //---------------------------------------------------------------------------
    @Override
    public void handleDeleteItem(DocumentSnapshot snapshot) {

        final DocumentReference documentReference = snapshot.getReference();
        final Luz luz = snapshot.toObject(Luz.class);

        documentReference.delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "onSuccess: Item deleted");
                    }
                });

        Snackbar.make(recyclerView, "Item deleted", Snackbar.LENGTH_LONG)
                .setAction("Undo", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        documentReference.set(luz);
                    }
                })
                .show();

    }


}
