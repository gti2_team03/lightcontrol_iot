package com.example.ellight.ui.perfil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.ellight.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class PerfilFragment extends Fragment {

    FirebaseUser usuario = FirebaseAuth.getInstance().getCurrentUser();
    TextView nombre;
    TextView correo;
    TextView telefono;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_perfil, container, false);

        nombre = (TextView) root.findViewById(R.id.nombre);
        correo = (TextView) root.findViewById(R.id.correo);
        telefono = (TextView) root.findViewById(R.id.telefono);

        cargarValores();

        return root;

    }

    public void cargarValores(){
        nombre.setText(usuario.getDisplayName());
        correo.setText(usuario.getEmail());
        telefono.setText(usuario.getPhoneNumber());
    }
}

