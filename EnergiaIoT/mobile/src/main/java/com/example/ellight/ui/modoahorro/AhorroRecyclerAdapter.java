package com.example.ellight.ui.modoahorro;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.example.ellight.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AhorroRecyclerAdapter extends FirestoreRecyclerAdapter<Ahorro, AhorroRecyclerAdapter.AhorroViewHolder> {

    private static final String TAG = "AhorroRecyclerAdapter";
    AhorroListener AhorroListener;

    public AhorroRecyclerAdapter(@NonNull FirestoreRecyclerOptions<Ahorro> options, AhorroListener AhorroListener) {
        super(options);
        this.AhorroListener = AhorroListener;
    }

    @Override
    protected void onBindViewHolder(@NonNull AhorroViewHolder holder, int position, @NonNull Ahorro Ahorro) {
        holder.AhorroTextView.setText(Ahorro.getNombre());
        holder.AhorroCheckBox.setChecked(Ahorro.getVital());
    }

    @Override
    public AhorroViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.ahorro_row, parent, false);
        return new AhorroViewHolder(view);
    }
    class AhorroViewHolder extends RecyclerView.ViewHolder {
        TextView AhorroTextView, dateTextView;
        CheckBox AhorroCheckBox;
        public AhorroViewHolder(@NonNull View itemView) {
            super(itemView);
            AhorroTextView = itemView.findViewById(R.id.ahorroTextView);
            AhorroCheckBox = itemView.findViewById(R.id.ahorroCheckBox);
            AhorroCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    DocumentSnapshot snapshot = getSnapshots().getSnapshot(getAdapterPosition());
                    Ahorro Ahorro = getItem(getAdapterPosition());
                    if (Ahorro.getVital() != isChecked) {
                        AhorroListener.handleCheckChanged(isChecked, snapshot);
                    }
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    DocumentSnapshot snapshot = getSnapshots().getSnapshot(getAdapterPosition());
                    AhorroListener.handleEditAhorro(snapshot);
                }
            });
        }
        public void deleteItem() {
            AhorroListener.handleDeleteItem(getSnapshots().getSnapshot(getAdapterPosition()));
        }
    }
    interface AhorroListener {
        public void handleCheckChanged(boolean isChecked, DocumentSnapshot snapshot);
        public void handleEditAhorro(DocumentSnapshot snapshot);
        public void handleDeleteItem(DocumentSnapshot snapshot);
    }
}