package com.example.ellight.ui.feedback;

public class Asking {

    private String ask;
    private String answ1;
    private String answ2;
    private String answ3;
    public Asking() {
    }

    public Asking(String ask, String  answ1, String answ2, String answ3) {
        this.ask = ask;
        this.answ1 = answ1;
        this.answ2 = answ2;
        this.answ2 = answ3;
    }

    @Override
    public String toString() {
        return "Asking{" +
                "ask='" + ask + '\'' +
                ", answ1='" + answ1 + '\'' +
                ", answ2='" + answ2 + '\'' +
                ", answ3='" + answ3 + '\'' +
                '}';
    }

    public String getAsk() {
        return ask;
    }

    public void setAsk(String ask) {
        this.ask = ask;
    }

    public String getAnsw1() {
        return answ1;
    }

    public void setAnsw1(String answ1) {
        this.answ1 = answ1;
    }

    public String getAnsw2() {
        return answ2;
    }

    public void setAnsw2(String answ2) {
        this.answ2 = answ2;
    }

    public String getAnsw3() {
        return answ3;
    }

    public void setAnsw3(String answ3) {
        this.answ3 = answ3;
    }
}
