package com.example.ellight.ui.controllight;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.ellight.MainActivity;

public class MyService extends Service {

    private Handler mHandler = new Handler();
    @Override
    public IBinder onBind(Intent i) {
        return null;
    }

    @Override
    public void onCreate() {
        startRepeating();
    }

    public void startRepeating() {
        mToastRunnable.run();
    }

    public void stopRepeating() {
        mHandler.removeCallbacks(mToastRunnable);
    }

    private Runnable mToastRunnable = new Runnable() {
        ControllightActivity activity = ControllightActivity.instance;
        @Override
        public void run() {
            if (activity != null) {
                // we are calling here activity's method
                activity.showToast();
            }
            mHandler.postDelayed(this, 1000);
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ControllightActivity activity = ControllightActivity.instance;
        if (activity != null) {
            activity.showToast();
        }
        return START_STICKY;
    }

}
