package com.example.ellight.ui.facture;

import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian3d;
import com.anychart.core.cartesian.series.Area3d;
import com.anychart.data.Mapping;
import com.anychart.data.Set;
import com.anychart.enums.Anchor;
import com.anychart.enums.HoverMode;
import com.anychart.enums.Position;
import com.anychart.enums.TooltipPositionMode;
import com.example.ellight.R;
import com.example.ellight.ui.graphics.GraphicsFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class FactureFragment extends Fragment {

    final FirebaseFirestore db = FirebaseFirestore.getInstance();
    final FirebaseUser usuario = FirebaseAuth.getInstance().getCurrentUser();

    public List<DataEntry> dataDec = new ArrayList<>();
    public List<DataEntry> dataJan = new ArrayList<>();

    String finalNodo;

    public String [] pruebaFechasDiciembre2019 = new String[999];
    public Double [] pruebaValoresDiciembre2019 = new Double[999];

    public String [] pruebaFechasEnero2020 = new String[999];
    public Double [] pruebaValoresEnero2020 = new Double[999];

    public Button btn_right;
    public Button btn_left;
    public TextView month;

    int contadorDic = 0;
    int contadorEne = 0;

    private AnyChartView anyChartView;
    private Set set;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_facture, container, false);

        final AnyChartView anyChartView1 = root.findViewById(R.id.any_chart_view);

        btn_right = root.findViewById(R.id.btn_right);
        btn_left = root.findViewById(R.id.btn_left);
        month = root.findViewById(R.id.txt_month);

        btn_left.setEnabled(false);
        btn_right.setEnabled(false);

        anyChartView1.setProgressBar(root.findViewById(R.id.progress_bar));
        anyChartView = anyChartView1;

        db.collection("usuarios").document(usuario.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>(){
            @Override public void onComplete(@NonNull Task<DocumentSnapshot> task){
                if(task.isSuccessful()) {

                    finalNodo = task.getResult().getString("nodo");

                    obtenerDatos();

                } else {
                    Log.d("Esta fallando esto", "Fallando seccion 1", task.getException());
                }
            }
        });

        return root;
    }

    public void obtenerDatos(){
        db.collection("nodos").document(finalNodo).collection("gastos").orderBy("momento", Query.Direction.ASCENDING).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Timestamp valor1 = document.getTimestamp("momento");
                        Double valor2 = document.getDouble("precio");

                        Calendar cal = GregorianCalendar.getInstance();
                        //cal.setTimeInMillis(valor1.getSeconds() * 1000L);
                        Date date = new Date (valor1.getSeconds() * 1000L);
                        String datev2 = DateFormat.format("dd/MM/yyyy hh:mm aa", cal).toString();
                        cal.setTime(date);

                        if (cal.get(Calendar.MONTH) == Calendar.DECEMBER && cal.get(Calendar.YEAR) == 2019){
                            pruebaFechasDiciembre2019[contadorDic] = date.toString();
                            pruebaValoresDiciembre2019[contadorDic] = valor2;

                            contadorDic++;

                        } else if (cal.get(Calendar.MONTH) == Calendar.JANUARY && cal.get(Calendar.YEAR) == 2020){
                            pruebaFechasEnero2020[contadorEne] = date.toString();
                            pruebaValoresEnero2020[contadorEne] = valor2;

                            contadorEne++;
                        }

                    }
                } else {
                    Log.d("Esta fallando esto", "Fallando seccion 2", task.getException());
                }

                cargarGrafico();
            }
        });

    }

    public void cargarGrafico(){

        for(int i=0; i<=contadorDic; i++){
            pushearDatosDec(pruebaFechasDiciembre2019[i], pruebaValoresDiciembre2019[i]);
        }

        for(int i=0; i<=contadorEne; i++){
            pushearDatosEne(pruebaFechasEnero2020[i], pruebaValoresEnero2020[i]);
        }

        Cartesian3d area3d = AnyChart.area3d();

        area3d.xAxis(0).labels().format("${%Value}");

        area3d.animation(true);

        area3d.yAxis(0).title("Consumo en euros");
        area3d.xAxis(0).title("Año/Mes/Dia - Hora");
        area3d.xAxis(0).labels().padding(5d, 5d, 0d, 5d);

        set = Set.instantiate();
        set.data(dataDec);
        Mapping series1Data = set.mapAs("{ x: 'x', value: 'value' }");

        Area3d series1 = area3d.area(series1Data);
        series1.name("Precio en ese momento");
        series1.hovered().markers(false);

        area3d.tooltip()
                .position(Position.CENTER_TOP)
                .positionMode(TooltipPositionMode.POINT)
                .anchor(Anchor.LEFT_BOTTOM)
                .offsetX(5d)
                .offsetY(5d);

        area3d.interactivity().hoverMode(HoverMode.BY_X);
        area3d.zAspect("100%");

        anyChartView.setChart(area3d);

    }

    public void pushearDatosDec(String fecha, Double numero){
        dataDec.add(new CustomDataEntry(fecha, numero));
    }

    public void pushearDatosEne(String fecha, Double numero){
        dataJan.add(new CustomDataEntry(fecha, numero));
    }

    public class CustomDataEntry extends ValueDataEntry {
        CustomDataEntry(String x, Double value) {
            super(x, value);
        }
    }
}