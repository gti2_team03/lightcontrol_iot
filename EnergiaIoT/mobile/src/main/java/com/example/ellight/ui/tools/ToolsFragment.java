package com.example.ellight.ui.tools;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;

import com.example.ellight.PreferenciasActivity;
import com.example.ellight.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;

public class ToolsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.fragment_tools);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new PrefsFragment()).commit();

    }


    public static class PrefsFragment extends PreferenceFragment{
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            final Context context = getActivity();
            addPreferencesFromResource(R.xml.fragment_tools);
            SwitchPreference dayNightSwitch = (SwitchPreference) findPreference("modo_noche");
            dayNightSwitch.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    boolean isNightMode = (boolean) newValue;
                    AppCompatDelegate.setDefaultNightMode(isNightMode ? AppCompatDelegate.MODE_NIGHT_YES: AppCompatDelegate.MODE_NIGHT_NO);
                    Intent i = new Intent(getContext(),PreferenciasActivity.class);
                    startActivity(i);
                    getActivity().finish();
                    return true;
                }
            });
        }
    }


    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
                                         Preference preference) {

        // Initiating Dialog's layout when any sub PreferenceScreen clicked
        if (preference.getClass() == PreferenceScreen.class) {
            // Retrieving the opened Dialog
            Dialog dialog = ((PreferenceScreen) preference).getDialog();
            if (dialog == null) return false;

            initDialogLayout(dialog);   // Initiate the dialog's layout
        }
        return true;
    }

    private void initDialogLayout(Dialog dialog) {
        View fragmentView = getView();

        // Get absolute coordinates of the PreferenceFragment
        int fragmentViewLocation[] = new int[2];
        fragmentView.getLocationOnScreen(fragmentViewLocation);

        // Set new dimension and position attributes of the dialog
        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();
        wlp.x = fragmentViewLocation[0]; // 0 for x
        wlp.y = fragmentViewLocation[1]; // 1 for y
        wlp.width = fragmentView.getWidth();
        wlp.height = fragmentView.getHeight();

        dialog.getWindow().setAttributes(wlp);

        // Set flag so that you can still interact with objects outside the dialog
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
    }
}