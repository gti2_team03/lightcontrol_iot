package com.example.ellight.ui.info;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.example.ellight.R;

public class CompartirAppFragment extends Fragment {
    Button share;
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_compartirapp, container, false);

        share = vista.findViewById(R.id.btn_compartir);
//para hacer que funcione el boton de compartir
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String texto = getResources().getString(R.string.intent_compartir);

                intent.putExtra(Intent.EXTRA_TEXT, texto);

                startActivity(intent);
            }
        });
        // Inflate the layout for this fragment
        return vista;



    }
    public void lanzarIntentCompartir(View view){
        share = view.findViewById(R.id.btn_compartir);



    }
}


