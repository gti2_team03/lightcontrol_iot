package com.example.ellight.ui.home;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.ellight.AcercaDeActivity;
import com.example.ellight.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.List;
import java.util.Locale;

public class HomeFragment extends Fragment {

    private TextToSpeech myTTS;
    private SpeechRecognizer mySpeechRecogniser;

    FirebaseUser usuario = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    String nodoMac = "";
    String nodo = "";

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        Button info = root.findViewById(R.id.butInfo);

        db.collection("usuarios").document(usuario.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>(){
            @Override public void onComplete(@NonNull Task<DocumentSnapshot> task){
                if(task.isSuccessful()) {
                    nodoMac = task.getResult().getString("nodo");
                    db.collection("nodos").document(nodoMac).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if(task.isSuccessful()){
                                nodo = task.getResult().getString("nombre");
                            }
                        }
                    });
                }
            }
        });

        info.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                Intent i = new Intent(getActivity(), AcercaDeActivity.class);
                startActivity(i);
            }
        });

        //Mic
        FloatingActionButton fab = root.findViewById(R.id.mic_controller);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
                mySpeechRecogniser.startListening(intent);

                /*try{
                    startActivityForResult(intent, 123);
                } catch (ActivityNotFoundException a){
                    Toast.makeText(getContext(),"No aportado", Toast.LENGTH_LONG).show();
                }*/
            }
        });

        initializeTextToSpeech();
        initializeSpeechRecogniser();

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        if (pref.getBoolean("musica", true)) {
            ((TextView) root.findViewById(R.id.textView4)).setVisibility(View.VISIBLE);
        } else {
            ((TextView) root.findViewById(R.id.textView4)).setVisibility(View.GONE);
        }

        return root;
    }

    private void initializeSpeechRecogniser() {
        if(SpeechRecognizer.isRecognitionAvailable(getActivity())){
            mySpeechRecogniser = SpeechRecognizer.createSpeechRecognizer(getActivity());
            mySpeechRecogniser.setRecognitionListener(new RecognitionListener() {

                @Override public void onReadyForSpeech(Bundle params) {

                }
                @Override public void onBeginningOfSpeech() {

                }
                @Override public void onRmsChanged(float rmsdB) {

                }
                @Override public void onBufferReceived(byte[] buffer) {

                }
                @Override public void onEndOfSpeech() {

                }
                @Override public void onError(int error) {

                }

                @Override public void onResults(Bundle results) {
                    List<String> command = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                    processResults(command.get(0));
                }

                @Override public void onPartialResults(Bundle partialResults) {

                }
                @Override public void onEvent(int eventType, Bundle params) {

                }

            });
        }
    }

    private void processResults(String command) {
        command = command.toLowerCase();

        //Switch off command
            if(command.indexOf("turn") != -1){
                if(command.indexOf("off") != -1){
                    speak("You turn off the node" + nodo);
                    turnOff();
                }
        //Switch on command
                if(command.indexOf("on") != -1){
                    //if(command.indexOf(nodo) != -1){
                    speak("You turn on the node" + nodo);
                    turnOn();
                    //}
                }

        //Switch auto command
                if(command.indexOf("auto") != -1){
                    //if(command.indexOf(nodo) != -1){
                    speak("You turn auto the node" + nodo);
                    turnAuto();
                    //}
                }
            }
        //Help Command
            else if(command.indexOf("help") != -1){
                speak ("You asked for help. You are on a app to control some lights and I am a Assistant to manipulate the lights faster, you can use the next comands. Turn on plus a node name to switch on. Turn off plus a node name to switch off. Turning auto plus a node name to switch to the PIR");
            }

    }

    private void turnOff() {
        DocumentReference node = db.collection("nodos").document(nodoMac);
        node.update("automatico", false, "estado", false).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override public void onSuccess(Void aVoid) {
                Log.d("Cambio de off", "Cambiado");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("Fallo en off", "Esta fallando");
            }
        });
    }

    private void turnOn(){
        DocumentReference node = db.collection("nodos").document(nodoMac);
        node.update("automatico", false, "estado", true).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override public void onSuccess(Void aVoid) {
                Log.d("Cambio de on", "Cambiado");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("Fallo en on", "Esta fallando");
            }
        });
    }

    private void turnAuto(){
        DocumentReference node = db.collection("nodo").document(nodoMac);
        node.update("automatico", true).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override public void onSuccess(Void aVoid) {
                Log.d("Cambio de auto", "Cambiado");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("Fallo en auto", "Esta fallando");
            }
        });
    }

    private void initializeTextToSpeech() {
        myTTS = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (myTTS.getEngines().size() == 0){
                    Toast.makeText(getActivity(), "The engine for TTS is not working for no micro", Toast.LENGTH_LONG).show();
                } else {
                    myTTS.setLanguage(Locale.ENGLISH);
                    speak("Hi, I'm a voice assistant ready for work with you, please press the floating button when you want to tell me something, the command help me will tell you all the options you have");
                }
            }
        });
    }

    private void speak (String command){
        if(Build.VERSION.SDK_INT >= 21){
            myTTS.speak(command, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {
            myTTS.speak(command, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    @Override public void onPause(){
        super.onPause();
        myTTS.shutdown();
    }

    @Override public void onResume() {
        super.onResume();
        initializeSpeechRecogniser();
        initializeTextToSpeech();
    }


}