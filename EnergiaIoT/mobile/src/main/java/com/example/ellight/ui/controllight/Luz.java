package com.example.ellight.ui.controllight;

import android.util.Log;

import com.google.firebase.Timestamp;
import java.util.List;

public class Luz {

    private String nombre;
    private boolean estado;
    private Timestamp momento;
    private String userId;

    private String MAC;
    private boolean automatico, intensidad_auto, color_custom;
    private boolean updatedViaMobile = false;

    private List<Integer> color;



    public Luz() {
    }

    public Luz(String nombre, boolean estado, Timestamp momento, String userId) {
        this.nombre = nombre;
        this.estado = estado;
        this.momento = momento;
        this.userId = userId;
    }
    public List<Integer> getColor() {
        return color;
    }
    public void setColor(List<Integer> color) {
        this.color = color;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public boolean getEstado() {
        return estado;
    }
    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    public Timestamp getMomento() {
        return momento;
    }
    public void setMomento(Timestamp momento) {
        this.momento = momento;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Luz{" +
                "nombre='" + nombre + '\'' +
                ", estado=" + estado +
                ", momento=" + momento +
                ", userId='" + userId + '\'' +
                '}';
    }


    public String getMAC() {
        return MAC;
    }

    public void setMAC(String MAC) {
        this.MAC = MAC;
    }

    public boolean isAutomatico() {
        return automatico;
    }

    public void setAutomatico(boolean automatico) {
        this.automatico = automatico;
    }

    public boolean isIntensidad_auto() {
        return intensidad_auto;
    }

    public void setIntensidad_auto(boolean intensidad_auto) {
        this.intensidad_auto = intensidad_auto;
    }

    public boolean isColor_custom() {
        return color_custom;
    }

    public void setColor_custom(boolean color_custom) {
        this.color_custom = color_custom;
    }

    public boolean isUpdatedViaMobile() {
        return updatedViaMobile;
    }

    public void setUpdatedViaMobile(boolean updatedViaMobile) {
        this.updatedViaMobile = updatedViaMobile;
    }
}
