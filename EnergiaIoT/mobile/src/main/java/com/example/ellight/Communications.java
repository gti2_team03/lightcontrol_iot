package com.example.ellight;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.ellight.ui.controllight.ControllightActivity;
import com.example.ellight.ui.controllight.Luz;
import com.example.ellight.ui.controllight.NotificacionEncendido;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;

import java.util.List;

public class Communications {

    /**
     * Funcion para actualizar el esado de un nodo de firestore,
     * la actualizacion se propaga al nodo correspondiente tambien
     * @param mac
     * @param estado
     */
    public static void setNodeEstado(String mac, final boolean estado, final Context ctx){

        FirebaseFirestore fb = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        fb.setFirestoreSettings(settings);

        fb.collection("nodos").whereEqualTo("mac", mac).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    for(QueryDocumentSnapshot doc: task.getResult()){
                        Luz n = doc.toObject(Luz.class);
                        n.setUpdatedViaMobile(true);
                        n.setAutomatico(false);

                        n.setEstado(estado);
                        doc.getReference().set(n);
                        support(ctx);
                    }
                }
            }
        });
    }


    /**
     * Funcion para actualizar el encendido automatico de un nodo de firestore,
     * la actualizacion se propaga al nodo correspondiente tambien
     * @param mac
     * @param estado
     */
    public static void setNodeAuto(String mac, final boolean estado){

        FirebaseFirestore fb = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        fb.setFirestoreSettings(settings);

        fb.collection("nodos").whereEqualTo("mac", mac).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    for(QueryDocumentSnapshot doc: task.getResult()){
                        Luz n = doc.toObject(Luz.class);
                        n.setUpdatedViaMobile(true);

                        n.setEstado(false);
                        n.setAutomatico(estado);
                        doc.getReference().set(n);

                    }
                }
            }
        });
    }



    /**
     * Funcion para actualizar el nombre de un nodo en firestore,
     * la actualizacion se propaga al nodo correspondiente tambien
     *
     * @param mac
     * @param nombre
     */
    public static void setNodeName(String mac, final String nombre){

        FirebaseFirestore fb = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        fb.setFirestoreSettings(settings);

        fb.collection("nodos").whereEqualTo("mac", mac).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    for(QueryDocumentSnapshot doc: task.getResult()){
                        Luz n = doc.toObject(Luz.class);
                        n.setUpdatedViaMobile(true);

                        n.setNombre(nombre);
                        doc.getReference().set(n);
                    }
                }
            }
        });
    }


    /**
     * Funcion para actualizar el color RGB  de un nodo en firestore,
     * la actualizacion se propaga al nodo correspondiente tambien
     *
     * @param mac
     * @param rgb
     */
    public static void setNodeCustomColor(String mac, final List<Integer> rgb){

        FirebaseFirestore fb = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        fb.setFirestoreSettings(settings);

        fb.collection("nodos").whereEqualTo("mac", mac).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    for(QueryDocumentSnapshot doc: task.getResult()){
                        Luz n = doc.toObject(Luz.class);
                        n.setUpdatedViaMobile(true);

                        n.setColor(rgb);
                        n.setColor_custom(true);

                        doc.getReference().set(n);
                    }
                }
            }
        });
    }




    /**
     * Funcion para actualizar el estoad automatico de la regulacion de la intensidad de un nodo de firestore,
     * la actualizacion se propaga al nodo correspondiente tambien
     * @param mac
     * @param estado
     */
    public static void setNodeIntensityAuto(String mac, final boolean estado){

        FirebaseFirestore fb = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        fb.setFirestoreSettings(settings);

        fb.collection("nodos").whereEqualTo("mac", mac).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    for(QueryDocumentSnapshot doc: task.getResult()){
                        Luz n = doc.toObject(Luz.class);
                        n.setUpdatedViaMobile(true);

                        n.setIntensidad_auto(estado);
                        doc.getReference().set(n);
                    }
                }
            }
        });
    }


    public static void support(final Context c_context){

        FirebaseFirestore.getInstance().collection("nodos")
                .whereEqualTo("estado", true)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {


                        String nombres= "";
                        String control= "";
                        WriteBatch batch = FirebaseFirestore.getInstance().batch();
                        List<DocumentSnapshot> snapshotList = queryDocumentSnapshots.getDocuments();

                        //c_context.stopService(Communications.serviceIntent);
                        /*NotificationManager nm = (NotificationManager) c_context.getSystemService(Context.NOTIFICATION_SERVICE);
                        try {
                            nm.cancelAll();
                        } catch(NullPointerException ex){
                            ex.printStackTrace();
                            Log.e("Notificaciones", "Ninguna notificación que cancelar");
                        }
*/
                        for (DocumentSnapshot snapshot : snapshotList) {
                            Luz product = snapshot.toObject(Luz.class);
                            nombres= nombres + "  " + product.getNombre();
                            control= control + product.getNombre();
                            //Log.d(TAG, "onComplete: " + product.getNombre());
                            //batch.update(snapshot.getReference(),"estado",false);
                        }


                        Log.d("Completado", "onComplete: " + control);

                         Intent serviceIntent = new Intent(c_context, NotificacionEncendido.class);
                        serviceIntent.putExtra("nodos", nombres);
                        c_context.startService(serviceIntent);

                        if(control==""){
                            c_context.stopService(serviceIntent);
                        }else{
                            c_context.stopService(serviceIntent);
                            c_context.startService(serviceIntent);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });
        }



}
