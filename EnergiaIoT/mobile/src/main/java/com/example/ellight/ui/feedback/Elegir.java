package com.example.ellight.ui.feedback;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ellight.LoginActivity;
import com.example.ellight.R;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;

import java.util.ArrayList;
import java.util.List;

public class Elegir extends AppCompatActivity implements FirebaseAuth.AuthStateListener {
    PieChart pieChart;
    PieData pieData;
    PieDataSet pieDataSet;
    ArrayList pieEntries;
    ArrayList PieEntryLabels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.elector);
        pieChart = findViewById(R.id.pieChart);
        readDocument();
        updateDocument();
        pieDataSet = new PieDataSet(pieEntries, "");
        pieData = new PieData(pieDataSet);
        pieChart.setData(pieData);
        pieDataSet.setColors(ColorTemplate.JOYFUL_COLORS);
        pieDataSet.setSliceSpace(2f);
        pieDataSet.setValueTextColor(Color.WHITE);
        pieDataSet.setValueTextSize(10f);
        pieDataSet.setSliceSpace(5f);
        updateDocument();

        Button pollbuton1 = findViewById(R.id.poll1);
        pollbuton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                elector("A");
            }
        });

        Button pollbuton2 = findViewById(R.id.poll2);
        pollbuton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                elector("B");
            }
        });

        Button pollbuton3 = findViewById(R.id.poll3);
        pollbuton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                elector("C");
            }
        });


    }

    //----------------------------------------------

    //-----------------------------------------------




    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        if (firebaseAuth.getCurrentUser() == null) {
            startLoginActivity();
            return;
        }
    }
    private void startLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
    @Override
    protected void onStart() {
        super.onStart();
        FirebaseAuth.getInstance().addAuthStateListener(this);
    }
    //----------------------
    public void elector(String W){
        final String userID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        final Feedb ferdi = new Feedb(userID,W);
        FirebaseFirestore.getInstance().collection("pruebas")
                    .whereEqualTo("id", userID)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            int a=0;
                            WriteBatch batch = FirebaseFirestore.getInstance().batch();
                            List<DocumentSnapshot> snapshotList = queryDocumentSnapshots.getDocuments();
                            for (DocumentSnapshot snapshot : snapshotList) { a=a+1; }
                            if (a>=1){ Toast.makeText(getApplicationContext(),"Already voted. thank you",Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getApplicationContext(),"thank you for voting",Toast.LENGTH_SHORT).show();
                                FirebaseFirestore.getInstance()
                                        .collection("pruebas")
                                        .add(ferdi);
                            }
                        }
                    });
    }
    public void updateDocument() {
        FirebaseFirestore.getInstance().collection("pruebas")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<DocumentSnapshot> snapshotList = queryDocumentSnapshots.getDocuments();
                        int a= 0; String aa = "A";
                        int b =0; String bb = "B";
                        int c= 0; String cc = "C";
                        for (DocumentSnapshot snapshot : snapshotList) {
                            Feedb product = snapshot.toObject(Feedb.class);
                            String voto= product.getVoto();
                            if (voto.equals(aa)) { a=a+1; }
                            if (voto.equals(bb)) { b=b+1; }
                            if (voto.equals(cc)) { c=c+1; }
                        }
                        Log.w("la a es",  String.valueOf(a));
                        Log.w("la b es",  String.valueOf(b));
                        Log.w("la c es",  String.valueOf(c));
                        getEntries(a,b,c);
                        pieDataSet = new PieDataSet(pieEntries, "");
                        pieData = new PieData(pieDataSet);
                        pieChart.setData(pieData);
                        pieDataSet.setColors(ColorTemplate.JOYFUL_COLORS);
                        pieDataSet.setSliceSpace(2f);
                        pieDataSet.setValueTextColor(Color.WHITE);
                        pieDataSet.setValueTextSize(10f);
                        pieDataSet.setSliceSpace(5f);
                    }
                });
    }

    //----------------------------
    public void readDocument() {

        FirebaseFirestore.getInstance()
                .collection("pruebas2")
                .document("ENG")
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        TextView name = (TextView)findViewById(R.id.txtName);
                        Button p1_button = (Button)findViewById(R.id.poll1);
                        Button p2_button = (Button)findViewById(R.id.poll2);
                        Button p3_button = (Button)findViewById(R.id.poll3);
                        name.setText(documentSnapshot.getString("ask"));
                        p1_button.setText(documentSnapshot.getString("answ1"));
                        p2_button.setText(documentSnapshot.getString("answ2"));
                        p3_button.setText(documentSnapshot.getString("answ3"));

                    }
                });
    }

    //----------------------------
    private void getEntries(int a, int b, int c) {

        //-------------------------


        pieEntries = new ArrayList<>();
        pieEntries.add(new PieEntry(a, "1 option"));
        pieEntries.add(new PieEntry(b, "2 option"));
        pieEntries.add(new PieEntry(c, "3 option"));
    }
}