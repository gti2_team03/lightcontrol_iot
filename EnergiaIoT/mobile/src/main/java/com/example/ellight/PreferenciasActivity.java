package com.example.ellight;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.ellight.ui.tools.ToolsFragment;

public class PreferenciasActivity extends Activity {


    private static View view;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changePreferencesTheme();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
// then you use

        if (prefs.getBoolean("modo_noche", false)) {
            setTheme(R.style.DarkTheme);
        }else{
            setTheme(R.style.LightTheme);
        }
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new ToolsFragment())
                .commit();



    }
// prueba asi
    /*
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.activity_preferencias, container, false);
        } catch (InflateException e) {

        }

        return view;

    }*/

    public void changePreferencesTheme() {
        SharedPreferences preferenciaNoche = PreferenceManager.getDefaultSharedPreferences(this);

        boolean modonoche = preferenciaNoche.getBoolean("modo_noche", true);

        if (modonoche) {
            setTheme(R.style.DarkThemePreferences);
        } else {
            setTheme(R.style.LightTheme);
        }

    }
}