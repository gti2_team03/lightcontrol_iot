package com.example.ellight;

public class Usuario {
    private String nombre;
    private String correo;
    private String telefono;
    private String direccion;
    private String pais;
    private String nodo;
    private long inicioSesion;

    public Usuario (String nombre, String correo, String telefono, String direccion, String pais, String nodo, long inicioSesion) {
        this.nombre = nombre;
        this.correo = correo;
        this.telefono = telefono;
        this.direccion = direccion;
        this.pais = pais;
        this.nodo = nodo;
        this.inicioSesion = inicioSesion;
    }
    public Usuario (String nombre, String correo, String telefono, String direccion, String pais, String nodo) {
        this(nombre, correo, telefono, direccion, pais, nodo, System.currentTimeMillis());
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono (String telefono) {
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getNodo() {
        return nodo;
    }

    public void setNodo(String nodo) {
        this.nodo = nodo;
    }
}

