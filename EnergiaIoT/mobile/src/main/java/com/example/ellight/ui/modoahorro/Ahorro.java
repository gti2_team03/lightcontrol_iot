package com.example.ellight.ui.modoahorro;

import java.util.List;

public class Ahorro {

    private String nombre;
    private boolean vital;
    private String userId;
    public Ahorro() {
    }

    public Ahorro(String nombre, boolean vital, String userId) {
        this.nombre = nombre;
        this.vital = vital;
        this.userId = userId;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public boolean getVital() {
        return vital;
    }
    public void setVital(boolean vital) {
        this.vital = vital;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Importacia{" +
                "nombre='" + nombre + '\'' +
                ", vital=" + vital +
                ", userId='" + userId + '\'' +
                '}';
    }
}
