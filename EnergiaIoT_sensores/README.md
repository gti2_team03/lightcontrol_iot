# Energia_IoT_sensores

Repositorio con los sensores del proyecto IoT

En la carpeta de "iluminacion" se encuentra el código del nodo que maneja la iluminación mediante un microcontrolador ESP32. Mientras que en la carpeta "Receptor" se encuentra el código de un nodo con un microcontrolador M5Stack, que recibe la información del ESP32 y la envía por el puerto serie a otro dispositivo.
