
const int UDP_Port_1 = 3325;
const int UDP_Port_2 = 3326;

#include "Transmisor.h"
#include "Receptor.h"

  bool automatico = true;
  bool lecturaBloqueada = false; //Impide que cambie el valor hasta que este se haya procesado correctamente

  void setup() {
    
    //Se inicializa el hardware y se configura el monitor Serie
    initiate();
    //Se inicializa la conexión con la red WiFi
    start_connection();


  
  }
  
  void loop() {

    //Escucha por UDP datos a enviar al nodo principal(RasberryPI)
    datos_recibidos(UDP_Port_1);
    readSerial();
    
  }
