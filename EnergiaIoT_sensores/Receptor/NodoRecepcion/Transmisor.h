
#include <M5Stack.h>

#include "WiFi.h"
#include "AsyncUDP.h"

#include <ArduinoJson.h>


//WiFi config
char * ssid = "IoT_TEAM03";
char * passwd = "58912485";


//DATOS
int datos_estado_length = 1000; 
char datos_estado[1000];

String valor;

boolean rec_1=0; //Datos recividos?
boolean rec_2=0; //Datos recividos?

AsyncUDP udp_connection_1; 
AsyncUDP udp_connection_2;

 
 void initiate(){
  dacWrite (25,0); //Silencia el altavoz del M5

  M5.begin(); 
  M5.Lcd.setTextSize(2);

  Serial.begin(9600);
  
 }

 //Conexión UDP para los cambios de estado en los nodos de iluminación
 void readUDP_1(int port){
    if(udp_connection_1.listen(port)){
      
      //M5.Lcd.println("Escuchando UDP en la IP:"); 
      //M5.Lcd.println(WiFi.localIP());
      //M5.Lcd.println();

      //Lectura de datos por UDP
      udp_connection_1.onPacket([](AsyncUDPPacket packet) {
          int i=datos_estado_length;
          while(i--){*(datos_estado+i)=*(packet.data()+i);}
          rec_1=1;

        });
        
    }
 }

//Conexión UDP para mandar una orden de cambio a fijo/automático a los sensores de iluminación
void readUDP_2(int port){
    if(udp_connection_2.listen(port)){
      
      M5.Lcd.println("Escuchando UDP en la IP:"); 
      M5.Lcd.println(WiFi.localIP());
      M5.Lcd.println();

      //Lectura de datos por UDP
      udp_connection_2.onPacket([](AsyncUDPPacket packet) {
          int i=datos_estado_length;
          while(i--){*(datos_estado+i)=*(packet.data()+i);}
          rec_2=1;

        });
        
    }
 }


  
 void start_connection(){
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, passwd);
  
    if (WiFi.waitForConnectResult() != WL_CONNECTED) {
      //Serial.println("WiFi Fallido.");
      while(1) {
        delay(1000);
        }
      }
      else{/*Serial.println("WiFi conexión realizada.\n");*/}
      
      readUDP_1(UDP_Port_1); 
      readUDP_2(UDP_Port_2); 
      
      
 }


 //Envío el estado automatico/fijo al nodo manejador de la iluminación
  void sendStatus(String datos, int puerto){     
    udp_connection_2.broadcastTo(datos.c_str(), puerto); 
}


  //Procesado de datos recibidos
  void datos_recibidos(int Port){
    if (rec_1){ //Paquete recibido por el puerto 1
      rec_1=0; //mensaje procesado

      
      //udp_connection.broadcastTo("Recibido",Port); //envia confirmacion
      //valor=String(datos_estado);

      StaticJsonDocument<1000> jsonBufferRecv; //definición buffer para almacenar el objeto JSON
      DeserializationError err = deserializeJson(jsonBufferRecv, datos_estado);
      if(err) {return;}


      // Representar Datos en M5Stack ////////////////
     if((jsonBufferRecv["RetrieveData"].as<boolean>())!=NULL){
        boolean retData = jsonBufferRecv["RetrieveData"];
        String m_mac = jsonBufferRecv["MAC"].as<String>();

     }
     else {
      
        String autoText = jsonBufferRecv["Auto"].as<String>();
        if(autoText=="true") {autoText = "Modo Automatico";}
        else {autoText = "Modo Fijo";}
  
        valor =jsonBufferRecv["Node"].as<String>()+": "+jsonBufferRecv["Status"].as<String>();
  
        M5.Lcd.clear(); 
        M5.Lcd.setCursor(0,20);
        
        M5.Lcd.println(autoText); M5.Lcd.println(); 
        M5.Lcd.println(jsonBufferRecv["MAC"].as<String>());
        M5.Lcd.println(valor);
      }

      //Serial.println(jsonBufferRecv.as<String>());
      
        //Enviar por el monitor Serie1 /////////////////
        Serial.flush(); Serial.print(jsonBufferRecv.as<String>()); Serial.flush();
        
 
      
       
    }

    if(rec_2){ rec_2 = 0; 

        //Enviar RP
    } //Paquete recibido por el puerto 2
    
  }
