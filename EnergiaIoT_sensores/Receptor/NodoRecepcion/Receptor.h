
String datosSerie;

void readSerial(){
  if(Serial.available()){
      datosSerie = "";
      datosSerie = Serial.readString();
      sendStatus(datosSerie, UDP_Port_2);
      Serial.flush();

    //M5.Lcd.clear();
    //M5.Lcd.setCursor(10,10); M5.Lcd.println(datosSerie);


     // Representa en pantalla
     StaticJsonDocument<1000> jsonBuffer2; //definición buffer para almacenar el objeto JSON
     DeserializationError err = deserializeJson(jsonBuffer2, datosSerie);
     
     String autoText = jsonBuffer2["automatico"].as<String>();
     if(autoText=="true") {autoText = "Modo Automatico";}
     else {autoText = "Modo Fijo";}

     String estado = "Apagado";
     if (jsonBuffer2["estado"]){estado="Encendido";}
     
     valor =jsonBuffer2["nombre"].as<String>()+": "+estado;
     
     M5.Lcd.clear(); 
     M5.Lcd.setCursor(0,20);
        
     M5.Lcd.println(autoText); M5.Lcd.println(); 
     M5.Lcd.println(jsonBuffer2["MAC"].as<String>());
     M5.Lcd.println(valor);
    
      
    }
 
}
