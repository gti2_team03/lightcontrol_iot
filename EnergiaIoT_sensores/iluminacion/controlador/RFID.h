#include<SPI.h>
#include <MFRC522.h>

#define SS_PIN 5
#define RST_PIN 1//not used for esp32

MFRC522 rfid(SS_PIN, RST_PIN);
byte readCard[4];
byte bufferSize = sizeof(readCard);


//Inicia la captura de datos via SPI y la lectura RFID
void initiateRFID(){
 SPI.begin();
 rfid.PCD_Init();
}


//Lee RFID y devuelve un parametro que indicará si mantener encendidas las luces o no
bool readRFID(){

  String cardID="";
  bool result = false;
  
  if ( ! rfid.PICC_IsNewCardPresent()) {
    rfid.PICC_HaltA();
    return result;
  }
  if ( ! rfid.PICC_ReadCardSerial()) { 
    rfid.PICC_HaltA();
    return result;
  }
  //Serial.println(F("Scanned PICC's UID:"));
  for ( uint8_t i = 0; i < 4; i++) {
    readCard[i] = rfid.uid.uidByte[i];
    //Serial.print(readCard[i], HEX);
    
    cardID = cardID + String(readCard[i], HEX);
    
  }

  rfid.PICC_HaltA();

   if(cardID == "4f265c29"){ //Tarjeta registrada
      //Serial.println("Tarjeta correcta!! Encender luces");
     result=true;
    }

   //Serial.println("");
   //Serial.println("Resultado final: "+cardID);
  
  
  return result;

}
