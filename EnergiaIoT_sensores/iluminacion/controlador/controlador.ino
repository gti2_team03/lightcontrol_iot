/* iIlumination
 * Control de presencia y actuador para conectar la ilumunación
 * @author: Adrián, García Andru
 */
 
#include "WiFi.h"
#include "AsyncUDP.h"
#include <ArduinoJson.h>

#define WIFI_PIN 27

const int UDP_Port_1 = 3325; //Transmision
const int UDP_Port_2 = 3326; //Recepción

int val; //valor leido por el PIR

bool automatico = true; //luces automáticas
bool auto_enviado = true; //Determina si se ha notificado el estado automático


hw_timer_t * timer = NULL; //temporizador

#define TIMER_CONSTANTE 1000000
#define TIMER_TIEMPO 1


////////////////////////////////////////////////
//////////FUNCIONES DE CONFIGURACIÓN //////////
//////////////////////////////////////////////

bool intensidad_variada = false; 

// Interrupción para controlar el tiempo con el que se actualiza 
// el control de intensidad en la iluminación.
void IRAM_ATTR onTimer(){
  intensidad_variada=false;
}


///// Conexión ////////////////
///////////////////////////////

char * ssid  = "IoT_TEAM03";
char * passwd = "58912485";
String nombreNodo = "";
String MAC = "";
String userId = "";


//Datos y buffer para intercambio de datos vía UDP

int datos_estado_length = 1000; 
char datos_estado[1000];


String valor;
boolean rec_1=0; //Datos recividos?
boolean rec_2=0;

AsyncUDP udp_connection_1, udp_connection_2; //sockets con conexión UDP


// Conexión UDP 1 (Socket utilizado para ENVIAR datos al nodo central)
 void readUDP_1(int port){

   //Escucha por un puerto
   if(udp_connection_1.listen(port)){
      Serial.print("Escuchando UDP por la IP: ");
      Serial.println(WiFi.localIP());
        udp_connection_1.onPacket([](AsyncUDPPacket packet) {
          Serial.write(packet.data(), packet.length()); Serial.println(); //Muestra por el puerto serie la respuesta enviada por el cliente via UDP          
          rec_1=1; //indica mensaje recibido
      });


     //Mando el estado por defecto a del sensor
      char datos_estado[1000]; StaticJsonDocument<1000> jsonDatos;    
      udp_connection_1.broadcastTo(datos_estado, port); 
   }
   
  
 }


// Conexión UDP 2 (Socket utilizado para RECIBIR datos al nodo central)
 void readUDP_2(int port){

   //Escucha por un puerto
   if(udp_connection_2.listen(port)){
      Serial.print("Escuchando UDP por la IP: ");
      Serial.println(WiFi.localIP());
        udp_connection_2.onPacket([](AsyncUDPPacket packet) {
          int i=datos_estado_length;
          while(i--){*(datos_estado+i)=*(packet.data()+i);}
          
          rec_2=1; //indica mensaje recibido
      });
      
   }
   
  
 }

boolean wifi_pin_status = false;
void checkWifi(){
  if(!wifi_pin_status){
    digitalWrite(WIFI_PIN, HIGH);
    wifi_pin_status=true;
  } else {
    digitalWrite(WIFI_PIN, LOW); 
    wifi_pin_status = false;
    }
 }



 //Inicia la conexión con la red predefinida
 void start_connection(){

  
  //Conectar a una red WiFi
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, passwd);
  while(1){
      checkWifi();
    //Si no hay conexión
      if(WiFi.waitForConnectResult()!=WL_CONNECTED){
        Serial.println("Fallo al conectar con la red WiFi \"IoT_TEM03\"... ");  
        delay(500);
      }else { //Si hay conexión
         break; 
    }
         
   } //Fin bucle
  
  wifi_pin_status = true; digitalWrite(WIFI_PIN, HIGH);
 
  MAC = WiFi.macAddress();
  if(nombreNodo.length()<1){ nombreNodo = MAC;}
  
  readUDP_1(UDP_Port_1); readUDP_2(UDP_Port_2);
  
 }



////////// LIBRERÍAS /////////////////////////////
//////////////////////////////////////////////////

#include "Iluminacion.h" //Funciones de la iluminacion
#include "Intensidad.h" //Funciones para regular la intensidad
#include "Movimiento.h" //Funciones para controlar el programa mediante el sensor PIR
#include "RFID.h" //Funciones para controlar el programa mediante lecturas RFID





////////////////////////////////////////////////
///// Envío y Recepción de datos //////////////
//////////////////////////////////////////////
void sendStatus(String stat,  int port){
    char datos_estado[1000]; 
    StaticJsonDocument<1000> jsonDatos; //buffer de datos para enviar vía UDP


     calculateConsumtion(current_R, current_G, current_B); //calculo de consumo antes de notificar el encendido/apagado

     jsonDatos["MAC"] = MAC;  
     jsonDatos["Node"] = nombreNodo;  
     jsonDatos["Status"] = stat.c_str();
     jsonDatos["Auto"] = automatico; //Envío el estado, si es automatico o fijo

     jsonDatos["color_custom"] = custom_color;
     JsonArray jcolores = jsonDatos.createNestedArray("color");
     jcolores.add(current_R); jcolores.add(current_G); jcolores.add(current_B);
     
     jsonDatos["intensidad_auto"] = variable;
     jsonDatos["updatedViaMobile"] = false;

     //jsonDatos["Vatios"] = consumedWatts;

     serializeJson(jsonDatos, datos_estado); //sprintf(datos_estado, "%s: %s", nombre, "ON");
     
     udp_connection_1.broadcastTo(datos_estado, port); 
    //Envío
}


 //Procesado de datos recibidos
 void datos_recibidos(int Port){
    if(rec_1){ rec_1=0; }
     
     if (rec_2){
      rec_2=0;
      
      //Se procesan los cambios de estado
      Serial.println("Datos recibidos: ");
      Serial.println(datos_estado);
      
      StaticJsonDocument<1000> jsonBufferRecv; //definición buffer para almacenar el objeto JSON
      DeserializationError err = deserializeJson(jsonBufferRecv, datos_estado);
      if(err) {return;}

    
      // Representar Datos en M5Stack ////////////////
      String m_MAC = jsonBufferRecv["MAC"].as<String>();
      if(m_MAC==MAC){ //Si la mac de lo enviado por la red coincide con la del dispositivo se modifican sus parametros

        //Iluminación
        if(!jsonBufferRecv["automatico"]){ //Si se cambián a modo fijo
          automatico=false;
          if(jsonBufferRecv["estado"]){
            encenderTodo(variable, custom_color); 
            } //Se encienden las luces o se apagan en función del estado en la BBDD
          else{
            apagarTodo();
            }
         }
         else{
           automatico=true;
          }

        //Intensidad
        if(jsonBufferRecv["intensidad_auto"]){variable=true;} 
        else{variable=false;}
        

        //Color
        if(jsonBufferRecv["color_custom"]){
          Serial.println("Cambio de color");
          custom_color = true;
          int c_R,c_G,c_B;
          
          c_R = jsonBufferRecv["color"][0];
          c_G = jsonBufferRecv["color"][1];
          c_B = jsonBufferRecv["color"][2];
          
          regularColor(c_R,c_G,c_B);
          
        } else {custom_color = false;}
            
            
      }

       //Se establece como nombre del nodo lo que se haya designado en firestore, en caso de no ser así, se utiliza la MAC como nombre por defecto
       if(jsonBufferRecv["nombre"].as<String>().length()>0){
         nombreNodo = jsonBufferRecv["nombre"].as<String>();
       } else {
        nombreNodo = m_MAC;
       }

   
     memset(datos_estado, NULL, 255); 
    }
  }

/**
 * Función para solicitar a la red cual es
 * mi última configuración y mi estado.
 */
void requestInfo(String mac){
      char peticion[150]; 
      StaticJsonDocument<150> jsonDatos;    
      
      jsonDatos["MAC"] = MAC;
      jsonDatos["RetrieveData"] = true;
      
     serializeJson(jsonDatos, peticion);

     Serial.println("Solicitando información por el puerto UDP 1....");
     udp_connection_1.broadcastTo(peticion, UDP_Port_1); 

   
}


 /////////////////////////////////////////////////
 ///////////// FIN DE LA CONFIGURACIÓN //////////
 ///////////////////////////////////////////////



//Configuración inicial
void setup() {
    //Inicializar el monitor serie.
    Serial.begin(9600);
    Serial.println("Iniciando programa...");
    delay(500);

    pinMode(WIFI_PIN, OUTPUT);

    timer = timerBegin(0, 80, true); //inicializo el timer, enlazado al timer0, con un divisor de 80 (80MHz fq del procesador del timer)
    timerAlarmWrite(timer, TIMER_CONSTANTE * TIMER_TIEMPO, true); //cada segundo
    timerAttachInterrupt(timer, &onTimer, true);

    //Se inicia la lectura del PIR, de la intensidad y del lector de RFID
    initiatePIR();
    initiateIntensity();
    initiateRFID();


    Serial.println("Solicitar información para "+MAC+"...");
    requestInfo(MAC); //

 }
  
  
//Ejecución del programa
void loop() {
   
   noInterrupts(); //Sin intrrupciones

   if(readRFID()){      
      if(automatico){
        automatico=false; //Cambia a fijo
          if(!encendido){
            setIntensityEq(getReverseIntensityInByte(getIntensity()),10);
            encenderTodo(variable, custom_color);
            
            sendStatus("Encendido", UDP_Port_1);
            
            }
        }
       else{
        automatico=true; 
          if(encendido){
            apagarTodo(); sendStatus("Apagado", UDP_Port_1);
          }
            
        } //Cambia a automático

   }
   
    //Se capturan las lecturas del lector de RFID

 
   if(automatico){
    val= digitalRead(PIR_PIN); //lectura del estado 
    lecturaPIR(val, LED_PIN); 
   }

  //Timer de 1 segundos
   timerAlarmEnable(timer);
   
  if(encendido){ //Solo se activan las interrupciones si la iluminación está encendida
      interrupts();
      setIntensityEq(getReverseIntensityInByte(getIntensity()),10);
      encenderTodo(variable, custom_color);
      noInterrupts();
  }
   
   datos_recibidos(UDP_Port_2); //Se escucha por el socket 2
   
}



  
