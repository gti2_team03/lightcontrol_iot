/*
 * Librería para leer la intensidad
 */

#include <driver/adc.h>

#define int_analog_max 4500
#define int_analog_min 0

//Los valores están invertidos, porque a más intensidad natural, menor artificial
#define int_digital_max 0 
#define int_digital_min 255

/////////PIN 32 como entrada por defecto /////////////
//Se va a utilizar el ADC1 por el canal 4 

void initiateIntensity(){
  adc1_config_width(ADC_WIDTH_BIT_12); //Ancho del ADC de 12 bits de precisión
  adc1_config_channel_atten(ADC1_CHANNEL_4, ADC_ATTEN_DB_11); //Atenuación de 11 Db, responde bien en interior a las lecturas analógicas ofrecidas

}


/*
 * Lee la intensidad de la luz a través de un sensor con una LDR
 * en formato analógico de 0 a 1024. 
 */
int getIntensity(){
  int val = 0;  
  val = adc1_get_raw(ADC1_CHANNEL_4); //Pin 32
  
  return val;
}

/*
 * Através de un valor de intensidad (entero), retorna la equivalente a esta intensidad
 * Pero con una resolución de 1byte, de 0 a 255 valores
 */
int getReverseIntensityInByte(int intensity){
  int mapped = map(intensity, int_analog_min, int_analog_max, int_digital_min, int_digital_max);

  return mapped;
}
