#include <FastLED.h>

#define LED_PIN 33 //PIN para iluminación
#define NUM_LEDS 8 //Numero de luces

#define defaultLight 20 //Valor de la iluminación por defecto

bool encendido = false; //Estado de las luces
CRGB leds[NUM_LEDS];

int intensidadFija[3] = {defaultLight, defaultLight, defaultLight};
int intensidadVariablePrev[3] = {defaultLight, defaultLight, defaultLight};
int intensidadVariable[3] = {defaultLight, defaultLight, defaultLight};

int m_R, m_G, m_B=defaultLight; //Valores RGB modificados, valores para dar un color a la iluminación
int colores[3] = {m_R, m_G, m_B};

//RGB actualizados con cada modificación de color/intensidad, utilizados para calcular la media del consumo al apagar los LEDs
int current_R = 0;
int current_G = 0;
int current_B = 0;

bool variable = true; //Determina si la intensidad se regula automáticamente o no
bool custom_color = false;

double consumedWatts = 0.0;


//Apaga todas las luces
void apagarTodo() {
  
  encendido = false;
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB(0, 0, 0);
    FastLED.show();
  }

}


/*
   Módifica el valor de la intensidad en todas las luces de manera equánime
*/
void setIntensityEq(int intensity, int atten) {
  if (!intensidad_variada) {
    for (int i = 0; i < 3; i++) {
      intensidadVariablePrev[i] = intensidadVariable[i]; //Almaceno la intensidad actual
      intensidadVariable[i] = round(intensity / atten); //Se reemplaza hacía la intensidad a cambiar
    }    
    intensidad_variada = true;
  }

}


/*
   Función para regular color de la iluminación
   R, G,B: valor RGB del color
*/
void regularColor(int R, int G, int B) {
  //Valores actuales
  m_R = R; m_G = G; m_B = B;
  
  colores[0] = m_R;
  colores[1] = m_G; 
  colores[2] = m_B;
}

void regularColorConIntensidad(int *intensidad, int *colores, int *luces){
  for(int i=0; i<3; i++){
    luces[i] = ((intensidad[i]*colores[i])/defaultLight);
  }
  
}

/*
 *  En función de los colores encendidos, calcula un gasto en voltios y amperios
 *  Devuelve los vatios
 */
void calculateConsumtion(int R, int G, int B){
  double v=0.0;
  double i=0.0;

  //Calculo de voltaje e intensidad si se usa el led Rojo
  if(R>0){v=v+2.1; i=(i+(0.02*R)/255); }

  //Calculo de voltaje e intensidad si se usa el led Verde
  if(G>0){v=v+3.2; i=(i+(0.02*G)/255);}
    
  //Calculo de voltaje e intensidad si se usa el led Azul
  if(B>0){ v=v+3.2; i=(i+(0.02*B)/255); }
    
  double vatios = (v*i);
  consumedWatts = vatios;
}


/*
   Enciende todas las luces
   variable: parametro para regultar si la intensidad es fija o automática
*/
void encenderTodo(bool variable, bool color) {
  
  encendido = true; //Se marcan las luces como encendidas
  
  int R, G, B;
  int luces[3] = {0,0,0};
  
  //Si es variable utilizo los valores con la intensidad modificada, si no utilzo los valores por defecto
  if(!variable){ 
    for(int i; i<3; i++){
      luces[i]=intensidadFija[i];
        if(color){ //modifico el color
         luces[i]= colores[i];
        }
     }
  } else { 
    for(int i; i<3; i++){
        luces[i]=intensidadVariable[i];
        //Se modifica el color proporcionalmente acorde a la intensidad ideal según el sensor
        if(color) { regularColorConIntensidad(intensidadVariable, colores, luces); }
      }
  }  

  
   R = luces[0]; G = luces[1];B = luces[2];
  
   current_R = R;
   current_G = G;
   current_B = B;
   
  //CalculateConsumtion(R,G,B); //Se calcula el consumo que correspondería antes de encender las luces
   
  fill_solid( leds, NUM_LEDS, CRGB(R, G, B));
  FastLED.show();

}


/*
   Inicializa el apartado lúminico (Instancia los LED)
*/
void start_lights() {
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS);
  //Se limpia toda la iluminación para evitar que se queden LEDs díscolos
  apagarTodo(); FastLED.clear();
}
