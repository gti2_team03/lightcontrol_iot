

#define PIR_PIN 34 //PIN para el sensor 
uint8_t PIR_state = LOW; //Estado inicial del sensor (desactivado)



/*
 * Función para todos los parametros de inicialización del hardware 
 * y protocolos de comunicación
 */
  void initiatePIR(){
 
  pinMode(PIR_PIN, INPUT); //PIN datos PIR como entrada
  pinMode(LED_PIN, OUTPUT); //PIN de luz

   
  start_connection();
  start_lights();  
  }


 /**
  * Función para leer un sensor PIR,
  * como parametro recibe el estado del PIN a leer, la lectura obtenida, y el pin para actuar (iluminación)
  */
 void lecturaPIR(int val, int pin_luz){
    
   if(val==HIGH) { //Si el sensor detecta movimiento

       //Encender las luces
       setIntensityEq(getReverseIntensityInByte(getIntensity()),10);
       encenderTodo(variable, custom_color);
       
      if(PIR_state == LOW){ //Sí el estado almacenado es bajo, se cambia a alto (movimiento detectado)

          //Determina que hay movimiento
          PIR_state = HIGH;
          sendStatus("Encendido", UDP_Port_1); 
          Serial.println("Movimiento!"); 
        } 
  }
    else{

      //Apagar luz
      apagarTodo();
      FastLED.clear();
      
      if(PIR_state == HIGH){ //Sí el estado almacenado es alto, se cambia a bajo (movimiento dejado de detectar)

          //Determina que deja de haber movimiento
          PIR_state = LOW; 
          sendStatus("Apagado", UDP_Port_1);
          Serial.println("Movimiento terminado."); 
        } 
      
   }

 }
